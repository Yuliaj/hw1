package com.htp.amazon;

import com.htp.config.AmazonConfiguration;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ObjectCannedACL;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

import java.util.UUID;

@Service
public class AmazonUploadingFileServiceImpl implements AmazonUploadingFileService {

    public static final String AMAZON_IMAGE_LINK_TEMPLATE = "%s/%s/%s/%s/%s.jpg";

    private AmazonConfiguration amazonConfiguration;

    private S3Client s3Client;

    public AmazonUploadingFileServiceImpl(AmazonConfiguration amazonConfiguration, S3Client s3Client) {
        this.amazonConfiguration = amazonConfiguration;
        this.s3Client = s3Client;
    }

    @Override
    public String uploadFile(byte[] imageBytes, Long userId) {
        String imageUUID = UUID.randomUUID().toString();
        PutObjectResponse putObjectResponse = s3Client.putObject(
                PutObjectRequest.builder()
                        .bucket(amazonConfiguration.getBucket())
                        .contentType("image/jpeg")
                        .contentLength((long) imageBytes.length)
                        .acl(ObjectCannedACL.PUBLIC_READ)
                        .key(String.format("%s/%s/%s.jpg",
                                amazonConfiguration.getFolder(),
                                userId,
                                imageUUID))
                        .build(), RequestBody.fromBytes(imageBytes)
        );
        return generateImageLink(userId, imageUUID);
    }

    private String generateImageLink(Long userId, String imageUUID){
        return String.format(AMAZON_IMAGE_LINK_TEMPLATE,
                amazonConfiguration.getServerURL(),
                amazonConfiguration.getBucket(),
                amazonConfiguration.getFolder(),
                userId,
                imageUUID);
    }
}
