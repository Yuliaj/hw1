package com.htp.aspect;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class LogAspect {

//    private static final Logger log = Logger.getLogger(LogAspect.class);

    @Pointcut("execution(* *(..)) &&\n" +
            "(\n" +
            "    within(com.htp.dao.jdbcTemplate.*))")
            public void beforeRepositoryPointcut() {
    }


    @Before("beforeRepositoryPointcut()")
    public void logBefore(JoinPoint joinPoint) {
        log.info("Method " + joinPoint.getSignature().getName() + " start");
    }
    @AfterReturning("within(com.htp.dao.jdbcTemplate.*)" )
    public void doAccessCheck(JoinPoint joinPoint) {
        log.info("logAfterReturning() is running!");
        log.info("Method " + joinPoint.getSignature().getName() + " finished");
//        System.out.println("logAfterReturning() is running!");
//        System.out.println("Method " + joinPoint.getSignature().getName() + " finished");
    }

}
