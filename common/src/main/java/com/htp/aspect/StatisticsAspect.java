package com.htp.aspect;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Aspect
@Component
public class StatisticsAspect {
//    private static final Logger log = Logger.getLogger(StatisticsAspect.class);

    private static final Map<String, Integer> counter = new HashMap<>();

    public static Map<String, Integer> getCounter() {
        return Collections.unmodifiableMap(counter);
    }

    @After("within(com.htp.dao.jdbcTemplate.*)")
    public void count(JoinPoint jp) {
        String method = jp.getSignature().getName();
        if (!counter.containsKey(method)) {
            counter.put(method, 1);
        } else {
            counter.put(method, counter.get(method) + 1);
        }
    }

    @After("within(com.htp.dao.jdbcTemplate.*)")
    public void out() {
        log.info("Methods statistics. Number of calls: ");
        log.info(getCounter().entrySet().toString());
    }

}
