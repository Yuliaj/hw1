package com.htp.aspect;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class TimingAspect {
//    private static final Logger log = Logger.getLogger(TimingAspect.class);
    Long time;

    @Before("within(com.htp.dao.jdbcTemplate.*)")
    public void count(JoinPoint jp) {
        log.info("in " + jp.getSignature().getName() + " Method");
        time = System.currentTimeMillis();
    }

    @After("within(com.htp.dao.jdbcTemplate.*)")
    public void out(JoinPoint jp) {
        Long res = System.currentTimeMillis() - time;
        log.info("out of " + jp.getSignature().getName() + " Method in " + res + " ms");
        log.info("Overall time - " + res + "ms");
    }

}
