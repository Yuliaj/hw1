package com.htp.dao;

import com.htp.domain.Role;

import java.util.List;

public interface RoleDao {

    List<Role> findAll();

    List<Role> search(String searchParam);

    List<Role> findByUserId(Long userId);

    Role findOne(Long roleId);

    Role save(Role role);

    Role update(Role role);

    int delete(Long roleId);

    int addBatch(List<Role> roles);
}
