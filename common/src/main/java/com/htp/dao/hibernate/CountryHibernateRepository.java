package com.htp.dao.hibernate;

import com.htp.dao.hibernate.dao.HibernateCountryDao;
import com.htp.domain.hibernate.HibernateCountry;
import com.htp.domain.hibernate.HibernateUser;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Repository
public class CountryHibernateRepository implements HibernateCountryDao {
    private SessionFactory sessionFactory;

    private EntityManagerFactory entityManagerFactory;

    public CountryHibernateRepository(SessionFactory sessionFactory, EntityManagerFactory entityManagerFactory) {
        this.sessionFactory = sessionFactory;
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public List<HibernateCountry> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        return entityManager.createQuery("select country from HibernateCountry country order by country.id asc", HibernateCountry.class).getResultList();
    }

    @Override
    public HibernateCountry findOne(Long countryId) {
        try (Session session = sessionFactory.openSession()) {
            return session.find(HibernateCountry.class, countryId);
        }
    }
}
