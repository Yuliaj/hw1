package com.htp.dao.hibernate;

import com.htp.dao.hibernate.dao.HibernateLanguageDao;
import com.htp.domain.hibernate.HibernateLanguage;
import com.htp.domain.hibernate.HibernateUser;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Repository
public class LanguageHibernateRepository implements HibernateLanguageDao {
    private SessionFactory sessionFactory;

    private EntityManagerFactory entityManagerFactory;

    public LanguageHibernateRepository(SessionFactory sessionFactory, EntityManagerFactory entityManagerFactory) {
        this.sessionFactory = sessionFactory;
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public List<HibernateLanguage> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        return entityManager.createQuery("select language from HibernateLanguage language order by language.id asc", HibernateLanguage.class).getResultList();
    }

    @Override
    public HibernateLanguage findOne(Long languageId) {
        try (Session session = sessionFactory.openSession()) {
            return session.find(HibernateLanguage.class, languageId);
        }
    }
}
