package com.htp.dao.hibernate;

import com.htp.dao.hibernate.dao.HibernateMessageDao;
import com.htp.domain.hibernate.HibernateMessage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.util.List;
import java.util.Optional;

@Repository("messageHibernateRepository")
public class MessageHibernateRepository implements HibernateMessageDao {
    private SessionFactory sessionFactory;
    private EntityManagerFactory entityManagerFactory;

    public MessageHibernateRepository(SessionFactory sessionFactory, EntityManagerFactory entityManagerFactory) {
        this.sessionFactory = sessionFactory;
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public List<HibernateMessage> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        return entityManager.createQuery("select message from HibernateMessage message order by message.id asc", HibernateMessage.class).getResultList();
    }

    @Override
    public List<HibernateMessage> search(String searchParam) {
        try (Session session = sessionFactory.openSession()){
            Query query = session.createQuery("FROM HibernateMessage where content = :content").setParameter("content", searchParam);
            return query.getResultList();
        }
    }

    @Override
    public Optional<HibernateMessage> findById(Long messageId) {
        return Optional.ofNullable(findOne(messageId));
    }

    @Override
    public HibernateMessage findOne(Long messageId) {
        try (Session session = sessionFactory.openSession()){
            return session.find(HibernateMessage.class, messageId);
        }
    }

    @Override
    public HibernateMessage save(HibernateMessage message) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.saveOrUpdate(message);
            session.getTransaction().commit();
            return message;
        }
    }

    @Override
    public HibernateMessage update(HibernateMessage message) {
        return save(message);
    }

    @Override
    public int delete(Long messageId) {
//        try (Session session = sessionFactory.openSession()){
//            session.delete(findOne(messageId));
//        }

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        javax.persistence.Query deleteQuery = entityManager
                .createQuery("update HibernateMessage message set message.isDeleted = true where message.id = :messageId")
                .setParameter("messageId", messageId);
        int result = deleteQuery.executeUpdate();
        transaction.commit();
        return result;
    }

}
