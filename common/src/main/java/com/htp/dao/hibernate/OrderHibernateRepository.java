package com.htp.dao.hibernate;

import com.htp.dao.hibernate.dao.HibernateOrderDao;
import com.htp.domain.hibernate.HibernateOrder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.util.List;
import java.util.Optional;

@Repository("orderHibernateRepository")
public class OrderHibernateRepository implements HibernateOrderDao {

    private SessionFactory sessionFactory;

    private EntityManagerFactory entityManagerFactory;

    public OrderHibernateRepository(SessionFactory sessionFactory, EntityManagerFactory entityManagerFactory) {
        this.sessionFactory = sessionFactory;
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public List<HibernateOrder> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        return entityManager.createQuery("select order from HibernateOrder order order by order.id asc", HibernateOrder.class).getResultList();
    }

    @Override
    public List<HibernateOrder> search(String searchParam) {
        try (Session session = sessionFactory.openSession()){
            Query query = session.createQuery("FROM HibernateOrder where orderText = :order_text").setParameter("order_text", searchParam);
            return query.getResultList();
        }
    }

    @Override
    public Optional<HibernateOrder> findById(Long orderId) {
        return Optional.ofNullable(findOne(orderId));
    }

    @Override
    public HibernateOrder findOne(Long orderId) {
        try (Session session = sessionFactory.openSession()) {
            return session.find(HibernateOrder.class, orderId);
        }

    }

    @Override
    public HibernateOrder save(HibernateOrder order) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.saveOrUpdate(order);
            session.getTransaction().commit();
            return order;
        }
    }

    @Override
    public HibernateOrder update(HibernateOrder order) {
        return save(order);
    }

    @Override
    public int delete(Long orderId) {
//        try (Session session = sessionFactory.openSession()){
//            session.delete(findOne(orderId));
//        }

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        javax.persistence.Query deleteQuery = entityManager
                .createNativeQuery("update m_orders set is_deleted = true where id = :orderId")
                .setParameter("orderId", orderId);
        int result = deleteQuery.executeUpdate();
        transaction.commit();
        return result;
    }


}
