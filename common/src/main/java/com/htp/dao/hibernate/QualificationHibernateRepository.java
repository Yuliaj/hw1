package com.htp.dao.hibernate;

import com.htp.dao.hibernate.dao.HibernateQualificationDao;
import com.htp.domain.hibernate.HibernateQualification;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Repository
public class QualificationHibernateRepository implements HibernateQualificationDao {
    private SessionFactory sessionFactory;

    private EntityManagerFactory entityManagerFactory;

    public QualificationHibernateRepository(SessionFactory sessionFactory, EntityManagerFactory entityManagerFactory) {
        this.sessionFactory = sessionFactory;
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public List<HibernateQualification> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        return entityManager.createQuery("select qualification from HibernateQualification qualification order by qualification.id asc", HibernateQualification.class).getResultList();

    }
}
