package com.htp.dao.hibernate;

import com.htp.dao.hibernate.dao.HibernateRoleDao;
import com.htp.domain.hibernate.HibernateRole;
import com.htp.domain.hibernate.HibernateRole_;
import com.htp.domain.hibernate.HibernateUser;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository("roleHibernateRepository")
public class RoleHibernateRepository implements HibernateRoleDao {

    private SessionFactory sessionFactory;

    private EntityManagerFactory entityManagerFactory;

    public RoleHibernateRepository(SessionFactory sessionFactory, EntityManagerFactory entityManagerFactory) {
        this.sessionFactory = sessionFactory;
        this.entityManagerFactory = entityManagerFactory;
    }

    //class to test queries constructed with Criteria API
    @Override
    public List<HibernateRole> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<HibernateRole> query = criteriaBuilder.createQuery(HibernateRole.class);
        Root<HibernateRole> root = query.from(HibernateRole.class);
        Expression<Long> id = root.get(HibernateRole_.id);
        query.select(root)
                .where(
                        id.in(1,2,3,4,5,6)
                )
                .orderBy(criteriaBuilder.asc(id));
        TypedQuery<HibernateRole> findAllQuery = entityManager.createQuery(query);
        return findAllQuery.getResultList();
    }

    @Override
    public List<HibernateRole> search(String searchParam) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<HibernateRole> query = criteriaBuilder.createQuery(HibernateRole.class);
        Root<HibernateRole> root = query.from(HibernateRole.class);
        Expression<String> roleName = root.get(HibernateRole_.roleName);
        Expression<Long> id = root.get(HibernateRole_.id);
        query.select(root)
                .where(
                        criteriaBuilder.like(roleName, searchParam)
                )
                .orderBy(criteriaBuilder.asc(id));
        TypedQuery<HibernateRole> searchQuery = entityManager.createQuery(query);
        return searchQuery.getResultList();
    }

    @Override
    public List<HibernateRole> findByUserId(Long userId) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<HibernateRole> query = criteriaBuilder.createQuery(HibernateRole.class);
        Root<HibernateRole> root = query.from(HibernateRole.class);
        Expression<HibernateUser> user = root.get(HibernateRole_.user);
        Expression<Long> id = root.get(HibernateRole_.id);
        query.select(root)
                .where(
                        criteriaBuilder.equal(user, userId)
                )
                .orderBy(criteriaBuilder.asc(id));
        TypedQuery<HibernateRole> findByUserIdQuery = entityManager.createQuery(query);
        return findByUserIdQuery.getResultList();
    }


    @Override
    public HibernateRole findOne(Long roleId) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<HibernateRole> query = criteriaBuilder.createQuery(HibernateRole.class);
        Root<HibernateRole> root = query.from(HibernateRole.class);
        Expression<Long> id = root.get(HibernateRole_.id);
        query.select(root)
                .where(
                        criteriaBuilder.equal(id, roleId)
                )
                .orderBy(criteriaBuilder.asc(id));
        TypedQuery<HibernateRole> findOneQuery = entityManager.createQuery(query);
        return findOneQuery.getSingleResult();
    }

    @Override
    public HibernateRole save(HibernateRole role) {
        try (Session session = sessionFactory.openSession()){
            session.saveOrUpdate(role);
            return role;
        }
    }

    @Override
    public HibernateRole update(HibernateRole role) {
        //*****Criteria API Update****//
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaUpdate<HibernateRole> criteriaUpdate = criteriaBuilder.createCriteriaUpdate(HibernateRole.class);
        Root<HibernateRole> root = criteriaUpdate.from(HibernateRole.class);
        Expression<Long> id = root.get(HibernateRole_.id);
        criteriaUpdate.set("role_name", "ROLE_ADMIN")
                .where(criteriaBuilder.equal(id, role.getId()));
        Query query = entityManager.createQuery(criteriaUpdate);
        return (HibernateRole) query.getSingleResult();
    }

    @Override
    public int delete(Long roleId) {
        //*********Criteria API Delete***********//
//        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
//        CriteriaDelete<HibernateRole> criteriaDelete = criteriaBuilder.createCriteriaDelete(HibernateRole.class);
//        Root<HibernateRole> root = criteriaDelete.from(HibernateRole.class);
//        Expression<Long> id = root.get(HibernateRole_.id);
//        criteriaDelete.where(criteriaBuilder.equal(id, roleId));
//        Query query = entityManager.createQuery(criteriaDelete);
//        return query.executeUpdate();
//
        //
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query deleteQuery = entityManager
                .createQuery("update HibernateRole role set role.isDeleted = true where role.id = :roleId")
                .setParameter("roleId", roleId);
        return deleteQuery.executeUpdate();
    }
}
