package com.htp.dao.hibernate;

import com.htp.dao.hibernate.dao.HibernateUserDao;
import com.htp.domain.hibernate.HibernateUser;
import com.htp.domain.hibernate.HibernateUser_;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Slf4j
@Repository("userHibernateRepository")
public class UserHibernateRepository implements HibernateUserDao {

    private SessionFactory sessionFactory;

    private EntityManagerFactory entityManagerFactory;

    public UserHibernateRepository(SessionFactory sessionFactory, EntityManagerFactory entityManagerFactory) {
        this.sessionFactory = sessionFactory;
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public List<HibernateUser> findAll() {
        try(final Session session = sessionFactory.openSession()){
            Query<HibernateUser> findAllQuery = session.createNamedQuery("HibernateUser_findAll", HibernateUser.class);
            return findAllQuery.list();
        }

    }

    @Override
    public List<HibernateUser> search(String searchParam) {
        try (Session session = sessionFactory.openSession()){
            Query query = session.createQuery("FROM HibernateUser where login = :login").setParameter("login", searchParam);
            return query.getResultList();
        }
    }

    @Override
    public Optional<HibernateUser> findById(Long userId) {
        return Optional.ofNullable(findOne(userId));
    }

    @Override
    public HibernateUser findOne(Long userId) {
        try (Session session = sessionFactory.openSession()) {
            return session.find(HibernateUser.class, userId);
        }
    }

    //Method save is marked as @Transactional on service level
    @Override
    public HibernateUser save(HibernateUser user) {
        try (Session session = sessionFactory.openSession()) {
            session.saveOrUpdate(user);
            return user;
        }
    }

    //The two methods below(update and delete) represent manual usage of transactions
    @Override
    public HibernateUser update(HibernateUser user) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(user);
            transaction.commit();
            return user;
        }
    }

    @Override
    public int delete(Long userId) {
//        try (final Session session = sessionFactory.openSession()){
//            final Transaction transaction = session.beginTransaction();
//            HibernateUser hibernateUser = session.find(HibernateUser.class, userId);
//            session.delete(hibernateUser);
//            transaction.commit();
//        }
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        javax.persistence.Query deleteQuery = entityManager
                .createQuery("update HibernateUser user set user.isDeleted = true where user.id = :userId")
                .setParameter("userId", userId);
        int result = deleteQuery.executeUpdate();
        transaction.commit();
        return result;
    }

    //Method addBatch is marked as @Transactional on service level
    @Override
    public int addBatch(List<HibernateUser> users) {
        try (Session session = sessionFactory.openSession()){
            for (HibernateUser user: users) {
             session.save(user);
            }
            return users.size();
        }
    }

    // Using Criteria API to find user by login
    @Override
    public Optional<List<HibernateUser>> findByLogin(String username) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<HibernateUser> query = criteriaBuilder.createQuery(HibernateUser.class);
        Root<HibernateUser> root = query.from(HibernateUser.class);
        Expression<String> login = root.get(HibernateUser_.login);
        query.select(root)
                .where(
                        criteriaBuilder.like(login, username)
                )
                .orderBy(criteriaBuilder.asc(login));
        TypedQuery<HibernateUser> findByLoginQuery = entityManager.createQuery(query);
        return Optional.of(findByLoginQuery.getResultList());
    }


}
