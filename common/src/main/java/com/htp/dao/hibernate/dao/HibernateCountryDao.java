package com.htp.dao.hibernate.dao;

import com.htp.domain.hibernate.HibernateCountry;

import java.util.List;

public interface HibernateCountryDao {
    List<HibernateCountry> findAll();

    HibernateCountry findOne(Long countryId);
}
