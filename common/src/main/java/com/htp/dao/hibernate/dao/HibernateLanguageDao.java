package com.htp.dao.hibernate.dao;

import com.htp.domain.hibernate.HibernateLanguage;

import java.util.List;

public interface HibernateLanguageDao {
    List<HibernateLanguage> findAll();

    HibernateLanguage findOne(Long languageId);
}
