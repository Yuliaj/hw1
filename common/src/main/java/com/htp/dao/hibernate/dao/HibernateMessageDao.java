package com.htp.dao.hibernate.dao;

import com.htp.domain.hibernate.HibernateMessage;

import java.util.List;
import java.util.Optional;

public interface HibernateMessageDao {
    List<HibernateMessage> findAll();

    List<HibernateMessage> search(String searchParam);

    Optional<HibernateMessage> findById(Long messageId);

    HibernateMessage findOne(Long messageId);

    HibernateMessage save(HibernateMessage message);

    HibernateMessage update(HibernateMessage message);

    int delete(Long messageId);

}
