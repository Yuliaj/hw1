package com.htp.dao.hibernate.dao;

import com.htp.domain.hibernate.HibernateOrder;

import java.util.List;
import java.util.Optional;

public interface HibernateOrderDao {
    List<HibernateOrder> findAll();

    List<HibernateOrder> search(String searchParam);

    Optional<HibernateOrder> findById(Long orderId);

    HibernateOrder findOne(Long userId);

    HibernateOrder save(HibernateOrder order);

    HibernateOrder update(HibernateOrder order);

    int delete(Long orderId);

}
