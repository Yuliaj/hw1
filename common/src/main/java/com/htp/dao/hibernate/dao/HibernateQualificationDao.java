package com.htp.dao.hibernate.dao;

import com.htp.domain.hibernate.HibernateQualification;

import java.util.List;

public interface HibernateQualificationDao {
    List<HibernateQualification> findAll();
}
