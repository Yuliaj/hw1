package com.htp.dao.hibernate.dao;

import com.htp.domain.hibernate.HibernateRole;

import java.util.List;

public interface HibernateRoleDao {

    List<HibernateRole> findAll();

    List<HibernateRole> search(String searchParam);

    List<HibernateRole> findByUserId(Long userId);

    HibernateRole findOne(Long roleId);

    HibernateRole save(HibernateRole role);

    HibernateRole update(HibernateRole role);

    int delete(Long roleId);
}
