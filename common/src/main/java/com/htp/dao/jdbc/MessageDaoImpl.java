package com.htp.dao.jdbc;

import com.htp.dao.MessageDao;
import com.htp.domain.Message;
import com.htp.exceptions.ResourceNotFoundException;
import com.htp.util.DatabaseConfiguration;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//@Repository("messageDaoImpl")
public class MessageDaoImpl implements MessageDao {

    public static DatabaseConfiguration config = DatabaseConfiguration.getInstance();
    private static String ID = "id";
    private static String FROM = "from";
    private static String CONTENT = "content";
    private static String DATE_OF_CREATION = "date_of_creation";
    private static String IS_READ = "is_read";
    private static String TO = "to";

    private DataSource dataSource;

    public MessageDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Message> findAll() {
        final String findAllQuery = "select * from m_messages order by id desc";
        List<Message> resultList = new ArrayList<>();
        /*2. DriverManager should get connection*/
        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(findAllQuery)) {

            /*4. Execute query*/
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                /*6. Add parsed info into collection*/
                resultList.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return resultList;
    }

    @Override
    public List<Message> search(String searchParam) {
        final String findAllQueryForPrepared = "select * from m_messages where id > ? order by id desc"; //:{имя параметра}    ?
        List<Message> resultList = new ArrayList<>();
        /*2. DriverManager should get connection*/
        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(findAllQueryForPrepared)) {

            preparedStatement.setLong(1, Long.parseLong(searchParam));

            /*4. Execute query*/
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                /*6. Add parsed info into collection*/
                resultList.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return resultList;
    }

    @Override
    public Optional<Message> findById(Long messageId) {
        return Optional.ofNullable(findOne(messageId));
    }

    @Override
    public Message findOne(Long messageId) {
        final String findById = "select * from m_messages where id = ?";
        Message message = null;
        ResultSet resultSet = null;
        /*2. DriverManager should get connection*/
        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(findById);
        ) {

            preparedStatement.setLong(1, messageId);
            /*4. Execute query*/
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                /*6. Add parsed info into collection*/
                message = parseResultSet(resultSet);
            } else {
                throw new ResourceNotFoundException("Message with id " + messageId + " not found");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                resultSet.close();
            } catch (SQLException throwables) {
                System.out.println(throwables.getMessage());
            }
        }

        return message;
    }

    @Override
    public Message save(Message message) {
        final String insertQuery = "INSERT INTO m_messages (id, user_id, content," +
                "date_of_creation, is_read, to_user) " +
                "VALUES (?, ?, ?, ?, ?, ?)";
        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
             PreparedStatement lastInsertId = connection.prepareStatement("SELECT currval('m_messages_id_seq') as last_insert_id;");
        ) {
            preparedStatement.setLong(1, message.getId());
            preparedStatement.setLong(2, message.getUserId());
            preparedStatement.setString(3, message.getContent());
            preparedStatement.setTimestamp(4, message.getDateOfCreation());
            preparedStatement.setBoolean(5, message.isRead());
            preparedStatement.setLong(6, message.getTo());

            preparedStatement.executeUpdate();

            ResultSet set = lastInsertId.executeQuery();

            long insertedUserId = set.getInt("last_insert_id");
            return findOne(insertedUserId);

        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    @Override
    public Message update(Message message) {
        final String updateQuery = "update m_messages set id = ?, user_id = ?, content = ?, date_of_creation = ?, " +
                "is_read = ?, to_user = ? " +
                "where id = ?";
        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(updateQuery);
        ) {
            preparedStatement.setLong(1, message.getId());
            preparedStatement.setLong(2, message.getUserId());
            preparedStatement.setString(3, message.getContent());
            preparedStatement.setTimestamp(4, message.getDateOfCreation());
            preparedStatement.setBoolean(5, message.isRead());
            preparedStatement.setLong(6, message.getTo());


            preparedStatement.executeUpdate();

            return findOne(message.getId());

        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    @Override
    public int delete(Long messageId) {
        final String deleteQuery = "delete from m_messages where id = ?";


        int rows = 0;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery)) {

            preparedStatement.setInt(1, messageId.intValue());

            rows = preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return rows;

    }
    @Override
    public int addBatch(List<Message> messages) {
        final String batchQuery = "INSERT INTO m_users (id, username, surname, login, password, e_mail, created, changed, is_blocked)\n" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(batchQuery)) {
            for (Message message: messages) {
                preparedStatement.setLong(1, message.getId());
                preparedStatement.setLong(2, message.getUserId());
                preparedStatement.setString(3, message.getContent());
                preparedStatement.setTimestamp(4, message.getDateOfCreation());
                preparedStatement.setBoolean(5, message.isRead());
                preparedStatement.setLong(6, message.getTo());
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return messages.size();

    }

    private Message parseResultSet(ResultSet resultSet) throws SQLException{
        Message message = new Message();
        message.setId(resultSet.getLong(ID));
        message.setUserId(resultSet.getLong(FROM));
        message.setContent(resultSet.getString(CONTENT));
        message.setDateOfCreation(resultSet.getTimestamp(DATE_OF_CREATION));
        message.setRead(resultSet.getBoolean(IS_READ));
        message.setTo(resultSet.getLong(TO));

        return message;
    }
}
