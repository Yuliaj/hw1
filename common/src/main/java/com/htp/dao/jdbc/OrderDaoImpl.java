package com.htp.dao.jdbc;

import com.htp.dao.OrderDao;
import com.htp.domain.Order;
import com.htp.exceptions.ResourceNotFoundException;
import com.htp.util.DatabaseConfiguration;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//@Repository("orderDaoImpl")
public class OrderDaoImpl implements OrderDao {
    public static DatabaseConfiguration config = DatabaseConfiguration.getInstance();

    private static String ID = "id";
    private static String USER_ID = "user_id";
    private static String COST = "cost";
    private static String ORDER = "order_text";
    private static String SUBSCRIBERS = "subscribers";
    private static String COUNTRY_ID = "country_id";
    private static String LANGUAGE_ID = "language_id";
    private static String CREATED = "created";
    private static String IS_AVAILABLE = "is_available";
    private DataSource dataSource;

    public OrderDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Order> findAll() {
        final String findAllQuery = "select * from m_orders order by id desc";

        List<Order> resultList = new ArrayList<>();
        /*2. DriverManager should get connection*/
        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(findAllQuery)) {

            /*4. Execute query*/
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                /*6. Add parsed info into collection*/
                resultList.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return resultList;
    }

    @Override
    public List<Order> search(String searchParam) {
        final String findAllQueryForPrepared = "select * from m_orders where id > ? order by id desc"; //:{имя параметра}    ?
        List<Order> resultList = new ArrayList<>();
        /*2. DriverManager should get connection*/
        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(findAllQueryForPrepared)) {

            preparedStatement.setLong(1, Long.parseLong(searchParam));

            /*4. Execute query*/
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                /*6. Add parsed info into collection*/
                resultList.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return resultList;
    }

    @Override
    public Optional<Order> findById(Long orderId) {
        return Optional.ofNullable(findOne(orderId));
    }

    @Override
    public Order findOne(Long orderId) {
        final String findById = "select * from m_orders where id = ?";
        Order order = null;
        ResultSet resultSet = null;
        /*2. DriverManager should get connection*/
        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(findById);
        ) {

            preparedStatement.setLong(1, orderId);
            /*4. Execute query*/
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                /*6. Add parsed info into collection*/
                order = parseResultSet(resultSet);
            } else {
                throw new ResourceNotFoundException("Order with id " + orderId + " not found");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                resultSet.close();
            } catch (SQLException throwables) {
                System.out.println(throwables.getMessage());
            }
        }

        return order;
    }

    @Override
    public Order save(Order order) {
        final String insertQuery = "INSERT INTO m_orders (id, user_id, cost, order_text," +
                "subscribers, country_id, language_id, created, is_available)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
             PreparedStatement lastInsertId = connection.prepareStatement("SELECT currval('m_orders_id_seq') as last_insert_id;");
        ) {
            preparedStatement.setLong(1, order.getId());
            preparedStatement.setLong(2, order.getUserId());
            preparedStatement.setLong(3, order.getCost());
            preparedStatement.setString(4, order.getOrderText());
            preparedStatement.setLong(5, order.getSubscribers());
            preparedStatement.setLong(6, order.getCountryId());
            preparedStatement.setLong(7, order.getLanguageId());
            preparedStatement.setTimestamp(8, order.getCreated());
            preparedStatement.setBoolean(9, order.isAvailable());

            preparedStatement.executeUpdate();

            ResultSet set = lastInsertId.executeQuery();

            long insertedUserId = set.getInt("last_insert_id");
            return findOne(insertedUserId);

        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    @Override
    public Order update(Order order) {
        final String updateQuery = "update m_orders set id = ?, user_id = ?, cost = ?, order_text = ?, is_available = ?, " +
                "subscribers = ?, country_id = ?, language_id = ?, created = ? " +
                "where id = ?";

        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(updateQuery);
        ) {
            preparedStatement.setLong(2, order.getUserId());
            preparedStatement.setLong(3, order.getCost());
            preparedStatement.setString(4, order.getOrderText());
            preparedStatement.setLong(5, order.getSubscribers());
            preparedStatement.setLong(6, order.getCountryId());
            preparedStatement.setLong(7, order.getLanguageId());
            preparedStatement.setTimestamp(8, order.getCreated());
            preparedStatement.setBoolean(9, order.isAvailable());

            preparedStatement.setLong(10, order.getId());

            preparedStatement.executeUpdate();

            return findOne(order.getId());

        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    @Override
    public int delete(Long orderId) {
        final String deleteQuery = "delete from m_orders where id = ?";

        int rows = 0;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery)) {

            preparedStatement.setInt(1, orderId.intValue());

            rows = preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return rows;

    }
    @Override
    public int addBatch(List<Order> orders) {
        final String batchQuery = "INSERT INTO m_users (id, username, surname, login, password, e_mail, created, changed, is_blocked)\n" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(batchQuery)) {
            for (Order order: orders) {
                preparedStatement.setLong(1, order.getId());
                preparedStatement.setLong(2, order.getUserId());
                preparedStatement.setLong(3, order.getCost());
                preparedStatement.setString(4, order.getOrderText());
                preparedStatement.setLong(5, order.getSubscribers());
                preparedStatement.setLong(6, order.getCountryId());
                preparedStatement.setLong(7, order.getLanguageId());
                preparedStatement.setTimestamp(8, order.getCreated());
                preparedStatement.setBoolean(9, order.isAvailable());
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return orders.size();

    }


    private Order parseResultSet(ResultSet resultSet) throws SQLException{
        Order order = new Order();
        order.setId(resultSet.getLong(ID));
        order.setUserId(resultSet.getLong(USER_ID));
        order.setCost(resultSet.getLong(COST));
        order.setOrderText(resultSet.getString(ORDER));
        order.setSubscribers(resultSet.getLong(SUBSCRIBERS));
        order.setCountryId(resultSet.getLong(COUNTRY_ID));
        order.setLanguageId(resultSet.getLong(LANGUAGE_ID));
        order.setCreated(resultSet.getTimestamp(CREATED));
        order.setAvailable(resultSet.getBoolean(IS_AVAILABLE));

        return order;
    }
}
