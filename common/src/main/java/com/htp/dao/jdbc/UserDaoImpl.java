package com.htp.dao.jdbc;

import com.htp.dao.UserDao;
import com.htp.domain.User;
import com.htp.exceptions.ResourceNotFoundException;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//@Repository("userDaoImpl")
public class UserDaoImpl implements UserDao {
    public static final String USER_ID = "id";
    public static final String USER_USERNAME = "username";
    public static final String USER_SURNAME = "surname";
    public static final String USER_LOGIN = "login";
    public static final String USER_PASSWORD = "password";
    public static final String EMAIL = "e_mail";
    public static final String USER_CREATED = "created";
    public static final String USER_CHANGED = "changed";
    public static final String USER_IS_BLOCKED = "is_blocked";
    private DataSource dataSource;

    public UserDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public UserDaoImpl() {
    }

    @Override
    public List<User> findAll() {
        final String findAllQuery = "select * from m_users order by id desc";
        List<User> resultList = new ArrayList<>();
        /*2. DriverManager should get connection*/
        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(findAllQuery)) {

            /*4. Execute query*/
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                /*6. Add parsed info into collection*/
                resultList.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return resultList;
    }

    @Override
    public List<User> search(String searchParam) {
        final String findAllQueryForPrepared = "select * from m_users where id > ? order by id desc"; //:{имя параметра}    ?
        List<User> resultList = new ArrayList<>();
        /*2. DriverManager should get connection*/
        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(findAllQueryForPrepared)) {

            preparedStatement.setLong(1, Long.parseLong(searchParam));

            /*4. Execute query*/
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                /*6. Add parsed info into collection*/
                resultList.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return resultList;
    }

    @Override
    public Optional<User> findById(Long userId) {
        return Optional.ofNullable(findOne(userId));
    }

    @Override
    public User findOne(Long userId) {
        final String findById = "select * from m_users where id = ?";
        User user = null;
        ResultSet resultSet = null;
        /*2. DriverManager should get connection*/
        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(findById);
        ) {

            preparedStatement.setLong(1, userId);
            /*4. Execute query*/
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                /*6. Add parsed info into collection*/
                user = parseResultSet(resultSet);
            } else {
                throw new ResourceNotFoundException("User with id " + userId + " not found");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                resultSet.close();
            } catch (SQLException throwables) {
                System.out.println(throwables.getMessage());
            }
        }

        return user;
    }

    @Override
    public User save(User user) {

        final String insertQuery = "INSERT INTO m_users (username, surname, login, password, e_mail, created, changed, is_blocked)\n" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
             PreparedStatement lastInsertId = connection.prepareStatement("SELECT currval('m_users_id_seq') as last_insert_id;");
        ) {
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getEmail());
            preparedStatement.setTimestamp(6, user.getCreated());
            preparedStatement.setTimestamp(7, user.getChanged());
            preparedStatement.setBoolean(8, user.isBlocked());

            preparedStatement.executeUpdate();

            ResultSet set = lastInsertId.executeQuery();
            set.next();
            long insertedUserId = set.getInt("last_insert_id");
            return findOne(insertedUserId);

        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    @Override
    public User update(User user) {
        final String updateQuery = "update m_users set username = ?, surname = ?, birth_date = ?, login = ?, password = ?, " +
                "created = ?, changed = ?, weight = ?, is_blocked = ? " +
                "where id = ?";
        try (Connection connection = dataSource.getConnection();
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(updateQuery);
        ) {
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getEmail());
            preparedStatement.setTimestamp(6, user.getCreated());
            preparedStatement.setTimestamp(7, user.getChanged());
            preparedStatement.setBoolean(8, user.isBlocked());

            preparedStatement.setLong(9, user.getId());

            preparedStatement.executeUpdate();

            return findOne(user.getId());

        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    @Override
    public int delete(Long userId) {
        final String deleteQuery = "delete from m_users where id = ?";
        int rows = 0;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery)) {

            preparedStatement.setInt(1, userId.intValue());

            rows = preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return rows;
    }

    @Override
    public int addBatch(List<User> users) {
        final String batchQuery = "INSERT INTO m_users (id, username, surname, login, password, e_mail, created, changed, is_blocked)\n" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(batchQuery)) {
            for (User user: users) {
                preparedStatement.setLong(1, user.getId());
                preparedStatement.setString(2, user.getUsername());
                preparedStatement.setString(3, user.getSurname());
                preparedStatement.setString(4, user.getLogin());
                preparedStatement.setString(5, user.getPassword());
                preparedStatement.setString(6, user.getEmail());
                preparedStatement.setTimestamp(7, user.getCreated());
                preparedStatement.setTimestamp(8, user.getChanged());
                preparedStatement.setBoolean(9, user.isBlocked());
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return users.size();

    }

    @Override
    public Optional<User> findByLogin(String username) {
        return Optional.empty();
    }

    private User parseResultSet(ResultSet resultSet) throws SQLException {
        User user = new User();
        /*5. Columns mapping*/
        user.setId(resultSet.getLong(USER_ID));
        user.setUsername(resultSet.getString(USER_USERNAME));
        user.setSurname(resultSet.getString(USER_SURNAME));
        user.setLogin(resultSet.getString(USER_LOGIN));
        user.setPassword(resultSet.getString(USER_PASSWORD));
        user.setEmail(resultSet.getString(EMAIL));
        user.setCreated(resultSet.getTimestamp(USER_CREATED));
        user.setChanged(resultSet.getTimestamp(USER_CHANGED));
        user.setBlocked(resultSet.getBoolean(USER_IS_BLOCKED));

        return user;
    }
}
