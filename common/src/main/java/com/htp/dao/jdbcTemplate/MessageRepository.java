package com.htp.dao.jdbcTemplate;

import com.htp.dao.MessageDao;
import com.htp.domain.Message;
import org.apache.log4j.Logger;

import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository("messageRepositoryJdbcTemplate")
public class MessageRepository implements MessageDao {
    private static final Logger log = Logger.getLogger(MessageRepository.class);

    private static final String ID = "id";
    private static final String USER_ID = "user_id";
    private static final String CONTENT = "content";
    private static final String DATE_OF_CREATION = "date_of_creation";
    private static final String IS_READ = "is_read";
    private static final String TO = "to_user";
    private static final String IS_DELETED = "is_deleted";

    private final JdbcTemplate jdbcTemplate;

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public MessageRepository(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    private Message messageRowMapper(ResultSet resultSet, int i) throws SQLException {
        Message message = new Message();
        message.setId(resultSet.getLong(ID));
        message.setUserId(resultSet.getLong(USER_ID));
        message.setContent(resultSet.getString(CONTENT));
        message.setDateOfCreation(resultSet.getTimestamp(DATE_OF_CREATION));
        message.setRead(resultSet.getBoolean(IS_READ));
        message.setTo(resultSet.getLong(TO));
        message.setDeleted(resultSet.getBoolean(IS_DELETED));
        return message;
    }


    @Override
    public List<Message> findAll() {
        final String findAllQuery = "select * from m_messages order by id desc";
        return jdbcTemplate.query(findAllQuery, this::messageRowMapper);
    }

    @Override
    public List<Message> search(String searchParam) {
        final String searchQuery = "select * from m_messages where content like :searchParam order by id desc";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("searchParam", searchParam);
        return namedParameterJdbcTemplate.query(searchQuery, namedParameters, this::messageRowMapper);
    }

    @Override
    public Optional<Message> findById(Long messageId) {
        return Optional.ofNullable(findOne(messageId));
    }

    @Override
    public Message findOne(Long messageId) {
        final String findOneQuery = "select * from m_messages where id = :messageId order by id desc\n";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("messageId", messageId);
        return namedParameterJdbcTemplate.queryForObject(findOneQuery, namedParameters, this::messageRowMapper);
    }

    @Override
    public Message save(Message message) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(message);
        final String insertQuery ="INSERT INTO m_messages (user_id, content, date_of_creation, is_read, to_user)"
                + " VALUES (:userId, :content, :dateOfCreation, :read, :to)";
        final String getLastId = "SELECT currval('m_messages_id_seq') as last_insert_id;";
        namedParameterJdbcTemplate.update(insertQuery, namedParameters);
        Long lastUserId = jdbcTemplate.queryForObject(getLastId, Long.class);
        return findOne(lastUserId);
    }

    @Override
    public Message update(Message message) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(message);
        final String updateQuery = "update m_messages set content = :content where id = :id;";
        namedParameterJdbcTemplate.update(updateQuery, namedParameters);
        return findOne(message.getId());
    }

    @Override
    public int delete(Long messageId) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        Long userId = findOne(messageId).getUserId();
        namedParameters.addValue("messageId", messageId);
        namedParameters.addValue("isDeleted", true);
        namedParameters.addValue("sender", userId);
        final String deleteQuery = "update m_messages set is_deleted = :isDeleted where id = :messageId and user_id = :sender";
        return namedParameterJdbcTemplate.update(deleteQuery, namedParameters);
    }

    @Override
    public int addBatch(List<Message> messages) {
        SqlParameterSource[] ids = new SqlParameterSource[messages.size()];
        final String batchQuery = "INSERT INTO m_messages (user_id, content, date_of_creation, is_read, to_user)"
                + " VALUES (:userId, :content, :dateOfCreation, :read, :to)";
        try {
            int index = 0;
            for (Message message : messages) {
                ids[index] = new BeanPropertySqlParameterSource(message);
                index ++;
            }
            //*
            return namedParameterJdbcTemplate.batchUpdate(batchQuery, ids).length;
        } catch (InvalidDataAccessApiUsageException e) {
            log.error("AddBatch method problems", e);
        }
        return 0;
    }
}
