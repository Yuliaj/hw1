package com.htp.dao.jdbcTemplate;

import com.htp.dao.OrderDao;
import com.htp.domain.Order;
import org.apache.log4j.Logger;

import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository("orderRepositoryJdbcTemplate")
public class OrderRepository implements OrderDao {

    private static final Logger log = Logger.getLogger(OrderRepository.class);
    private static final String ID = "id";
    private static final String USER_ID = "user_id";
    private static final String COST = "cost";
    private static final String ORDER = "order_text";
    private static final String SUBSCRIBERS = "subscribers";
    private static final String COUNTRY_ID = "country_id";
    private static final String LANGUAGE_ID = "language_id";
    private static final String CREATED = "created";
    private static final String IS_AVAILABLE = "is_available";
    private static final String IS_DELETED = "is_deleted";

    private final JdbcTemplate jdbcTemplate;

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public OrderRepository(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    private Order orderRowMapper(ResultSet resultSet, int i) throws SQLException {
        Order order = new Order();
        order.setId(resultSet.getLong(ID));
        order.setUserId(resultSet.getLong(USER_ID));
        order.setCost(resultSet.getLong(COST));
        order.setOrderText(resultSet.getString(ORDER));
        order.setSubscribers(resultSet.getLong(SUBSCRIBERS));
        order.setCountryId(resultSet.getLong(COUNTRY_ID));
        order.setLanguageId(resultSet.getLong(LANGUAGE_ID));
        order.setCreated(resultSet.getTimestamp(CREATED));
        order.setAvailable(resultSet.getBoolean(IS_AVAILABLE));
        order.setDeleted(resultSet.getBoolean(IS_DELETED));
        return order;
    }

    @Override
    public List<Order> findAll() {
        final String findAllQuery = "select * from m_orders order by id desc";
        return jdbcTemplate.query(findAllQuery, this::orderRowMapper);
    }

    @Override
    public List<Order> search(String searchParam) {
        final String searchQuery = "select * from m_orders where cost = :searchParam order by id desc";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("searchParam", Long.parseLong(searchParam));
        return namedParameterJdbcTemplate.query(searchQuery, namedParameters, this::orderRowMapper);
    }

    @Override
    public Optional<Order> findById(Long orderId) {
        return Optional.ofNullable(findOne(orderId));
    }

    @Override
    public Order findOne(Long orderId) {
        final String findOneQuery = "select * from m_orders where id = :orderId order by id desc\n";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("orderId", orderId);
//        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(orderId.getClass());
        return namedParameterJdbcTemplate.queryForObject(findOneQuery, namedParameters, this::orderRowMapper);

    }

    @Override
    public Order save(Order order) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(order);
        final String insertQuery ="INSERT INTO m_orders (user_id, cost, order_text, subscribers, country_id, language_id, created, is_available)"
                + " VALUES (:userId, :cost, :orderText, :subscribers, :countryId, :languageId, :created, :available)";
        final String getLastId = "SELECT currval('m_orders_id_seq') as last_insert_id;";
        namedParameterJdbcTemplate.update(insertQuery, namedParameters);
        Long lastUserId = jdbcTemplate.queryForObject(getLastId, Long.class);
        return findOne(lastUserId);
    }

    @Override
    public Order update(Order order) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(order);
        final String updateQuery = "update m_orders set order_text = :order_text where id = :id;";
        namedParameterJdbcTemplate.update(updateQuery, namedParameters);
        return findOne(order.getId());
    }

    @Override
    public int delete(Long orderId) {
//        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
//        namedParameters.addValue("orderId", orderId);
//        namedParameters.addValue("isDeleted", true);
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(orderId.getClass());
        final String deleteQuery = "update m_orders set is_deleted = :deleted where id = :id";
        return namedParameterJdbcTemplate.update(deleteQuery, namedParameters);
    }

    @Override
    public int addBatch(List<Order> orders) {
        SqlParameterSource[] ids = new SqlParameterSource[orders.size()];
        final String batchQuery = "INSERT INTO m_orders (user_id, cost, order_text, subscribers, country_id, language_id, created, is_available)"
                + " VALUES (:userId, :cost, :orderText, :subscribers, :countryId, :languageId, :created, :available)";
        try {
            int index = 0;
            for (Order order : orders) {
                ids[index] = new BeanPropertySqlParameterSource(order);
                index ++;
            }
            //*
            return namedParameterJdbcTemplate.batchUpdate(batchQuery, ids).length;
        } catch (InvalidDataAccessApiUsageException e) {
            log.error("AddBatch method problems", e);
        }
        return 0;
    }
}
