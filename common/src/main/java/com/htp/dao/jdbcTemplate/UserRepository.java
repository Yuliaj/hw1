package com.htp.dao.jdbcTemplate;

import com.htp.dao.UserDao;
import com.htp.domain.User;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository("userRepositoryJdbcTemplate")

public class UserRepository implements UserDao {

    private static final Logger log = Logger.getLogger(com.htp.dao.jdbcTemplate.UserRepository.class);

    private static final String USER_ID = "id";
    private static final String USER_USERNAME = "username";
    private static final String USER_SURNAME = "surname";
    private static final String USER_LOGIN = "login";
    private static final String USER_PASSWORD = "password";
    private static final String EMAIL = "e_mail";
    private static final String USER_CREATED = "created";
    private static final String USER_CHANGED = "changed";
    private static final String USER_IS_BLOCKED = "is_blocked";
    private static final String IS_DELETED = "is_deleted";

    private final JdbcTemplate jdbcTemplate;

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UserRepository(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public List<User> findAll() {
        final String findAllQuery = "select * from m_users order by id desc";
        return jdbcTemplate.query(findAllQuery, this::userRowMapper);

    }

    private User userRowMapper(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getLong(USER_ID));
        user.setUsername(resultSet.getString(USER_USERNAME));
        user.setSurname(resultSet.getString(USER_SURNAME));
        user.setLogin(resultSet.getString(USER_LOGIN));
        user.setPassword(resultSet.getString(USER_PASSWORD));
        user.setEmail(resultSet.getString(EMAIL));
        user.setCreated(resultSet.getTimestamp(USER_CREATED));
        user.setChanged(resultSet.getTimestamp(USER_CHANGED));
        user.setBlocked(resultSet.getBoolean(USER_IS_BLOCKED));
        user.setDeleted(resultSet.getBoolean(IS_DELETED));
        return user;
    }

    @Override
    public List<User> search(String searchParam) {
        final String searchQuery = "select * from m_users where login = :searchParam order by id desc";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("searchParam", searchParam);
        return namedParameterJdbcTemplate.query(searchQuery, namedParameters, this::userRowMapper);
    }

    @Override
    public Optional<User> findById(Long userId) {
        return Optional.ofNullable(findOne(userId));
    }

    @Override
    public User findOne(Long userId) {
        final String findOneQuery = "select * from m_users where id = :userId order by id desc\n";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("userId", userId);
        return namedParameterJdbcTemplate.queryForObject(findOneQuery, namedParameters, this::userRowMapper);

    }

    @Override
    public User save(User user) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(user);
        final String insertQuery ="INSERT INTO m_users (username, surname, login, password, e_mail, created, changed, is_blocked)"
                + " VALUES ( :username, :surname, :login, :password, :email, :created, :changed, :blocked)";
        namedParameterJdbcTemplate.update(insertQuery, namedParameters);
        final String getLastId = "SELECT currval('m_users_id_seq') as last_insert_id;";
        Long lastUserId = jdbcTemplate.queryForObject(getLastId, Long.class);
        return findOne(lastUserId);
    }

    @Override
    public User update(User user){
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(user);
        final String updateQuery = "update m_users set username = :username where id = :id;";
        namedParameterJdbcTemplate.update(updateQuery, namedParameters);
        return findOne(user.getId());
    }

    @Override
    public int delete(Long userId) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("userId", userId);
        namedParameters.addValue("isDeleted", true);
        final String deleteQuery = "update m_users set is_deleted = :isDeleted where id = :userId";
        return namedParameterJdbcTemplate.update(deleteQuery, namedParameters);
    }

    @Override
    public int addBatch(List<User> users) {
        SqlParameterSource[] ids = new SqlParameterSource[users.size()];
        final String batchQuery = "INSERT INTO m_users (id, username, surname, login, password, e_mail, created, changed, is_blocked)\n" +
                "VALUES (:id, :username, :surname, :login, :password, :email, :created, :changed, :blocked)";
        try {
            int index = 0;
            for (User user : users) {
                ids[index] = new BeanPropertySqlParameterSource(user);
                index ++;
            }
            //*
            return namedParameterJdbcTemplate.batchUpdate(batchQuery, ids).length;
        } catch (InvalidDataAccessApiUsageException e) {
            log.error("AddBatch method problems", e);
        }
        return 0;
    }

    @Override
    public Optional<User> findByLogin(String username) {
        try {
            final String findByLogin = "select * from m_users where login = :username order by id desc\n";
            MapSqlParameterSource namedParams = new MapSqlParameterSource();
            namedParams.addValue("username", username);
            return Optional.of(namedParameterJdbcTemplate.queryForObject(findByLogin,namedParams, this::userRowMapper));
        } catch (DataAccessException ex){
            return Optional.empty();
        }

    }
}
