package com.htp.dao.spring_data;

import com.htp.domain.hibernate.HibernateMessage;
import com.htp.profile.MessageStatisticsProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SpringDataMessageRepository extends CrudRepository<HibernateMessage, Long>,
        JpaRepository<HibernateMessage, Long>, PagingAndSortingRepository<HibernateMessage, Long> {
    //Business-cases
    Integer countHibernateMessageByContentContains(String str);

    HibernateMessage findByIdIs(Long messageId);


//    void createChatWithManyPeople();

    //business case number 1
    @Query(value = "select content, count(*) as count" +
                   " from m_messages m inner join m_users u on m.user_id = u.id " +
                   " where u.id = :userId" +
                   " group by content " +
                   " order by count(content) desc", nativeQuery = true)
    List<MessageStatisticsProfile> getHibernateMessageStatisticsForUser(@Param("userId") Long userId);

    //business case number 2
    @Query(value = "select content, count(*) as count" +
            " from m_messages m inner join m_users u on m.user_id = u.id " +
            " group by content " +
            " order by count(content) desc", nativeQuery = true)
    List<MessageStatisticsProfile> getHibernateMessageStatisticsOverall();

}
