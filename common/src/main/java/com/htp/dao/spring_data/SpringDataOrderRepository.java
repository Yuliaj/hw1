package com.htp.dao.spring_data;

import com.htp.domain.hibernate.HibernateOrder;
import com.htp.profile.OrderStatisticsProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SpringDataOrderRepository extends CrudRepository<HibernateOrder, Long>,
        JpaRepository<HibernateOrder, Long>, PagingAndSortingRepository<HibernateOrder, Long> {

    //business case number 1
    @Query(value = "select u.login, count(*) as count" +
            " from m_orders m inner join m_users u on m.user_id = u.id " +
            " where u.id = :userId" +
            " group by u.login " +
            " order by count(*)", nativeQuery = true)
    OrderStatisticsProfile getHibernateOrderStatisticsForUser(@Param("userId") Long userId);

    //business case number 2
    @Query(value = "select u.login, count(*) as count" +
            " from m_orders m inner join m_users u on m.user_id = u.id " +
            " group by u.login " +
            " order by count(*) desc", nativeQuery = true)
    List<OrderStatisticsProfile> getHibernateOrderStatisticsForAllUsers();
}
