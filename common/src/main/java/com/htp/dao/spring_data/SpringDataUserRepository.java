package com.htp.dao.spring_data;

import com.htp.domain.hibernate.HibernateUser;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

@CacheConfig(cacheNames = {"usersAdmins"})
public interface SpringDataUserRepository extends CrudRepository<HibernateUser, Long>,
        JpaRepository<HibernateUser, Long>, PagingAndSortingRepository<HibernateUser, Long> {
    

    @Cacheable
    HibernateUser findByLoginIs(String login);

    HibernateUser findByIdIs(Long id);
    //business-case 1
    @Modifying
    @Query("update HibernateUser u set u.password = :newPassword where u.id = :userId")
    Integer resetPassword(String newPassword, Long userId);

    //business-case 2
    @Modifying
    @Query("update HibernateUser u set u.imageLink = :imageLink where u.id = :userId")
    Integer updateImageLink(String imageLink, Long userId);
}
