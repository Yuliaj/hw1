package com.htp.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Message implements Serializable {

    @NotNull
    private Long id;

    @NotNull
    private Long userId;

    @NotEmpty
    @NotBlank
    @Size(max = 5000)
    private String content;


    private Timestamp dateOfCreation;


    private boolean isRead;

    @NotNull
    private Long to;

    private boolean isDeleted;

    public Message() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Timestamp dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        this.isRead = read;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return isRead == message.isRead &&
                isDeleted == message.isDeleted &&
                Objects.equals(id, message.id) &&
                Objects.equals(userId, message.userId) &&
                Objects.equals(content, message.content) &&
                Objects.equals(dateOfCreation, message.dateOfCreation) &&
                Objects.equals(to, message.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, content, dateOfCreation, isRead, to, isDeleted);
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", userId=" + userId +
                ", content='" + content + '\'' +
                ", dateOfCreation=" + dateOfCreation +
                ", isRead=" + isRead +
                ", to=" + to +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
