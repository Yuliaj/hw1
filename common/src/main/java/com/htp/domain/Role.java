package com.htp.domain;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

//@Builder
public class Role implements Serializable {
    @NotNull
    private Long id;

    @NotNull
    private Long userId;

    @NotEmpty
    @NotBlank
    private String roleName;

    private boolean isDeleted;

    public Role() {
    }

    public Role(Long userId, String roleName) {
        this.userId = userId;
        this.roleName = roleName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return Objects.equals(userId, role.userId) &&
                Objects.equals(roleName, role.roleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, roleName);
    }

    @Override
    public String toString() {
        return "Role{" +
                "userId=" + userId +
                ", roleName='" + roleName + '\'' +
                '}';
    }
}
