package com.htp.domain;

import org.springframework.stereotype.Component;

import java.net.http.HttpClient;

@Component
public class TestSpring {

    private HttpClient httpClient;

    public TestSpring(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public HttpClient getHttpClient() {
        return httpClient;
    }

    public void setHttpClient(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    public String toString() {
        return "TestSpring{" +
                ", httpClient=" + httpClient +
                '}';
    }
}
