package com.htp.domain.hibernate;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collections;
import java.util.Set;

@Setter
@Getter
@EqualsAndHashCode(exclude = {
        "orders"
})
@ToString(exclude = {
        "orders"
})
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "m_languages")
public class HibernateLanguage implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @NotBlank
    @Column
    @Size(max = 200)
    private String name;

    @NotEmpty
    @NotBlank
    @Column
    @Size(max = 50)
    private String abbreviation;

    @JsonManagedReference
    @OneToMany(mappedBy = "language", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<HibernateOrder> orders = Collections.emptySet();
}
