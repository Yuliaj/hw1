package com.htp.domain.hibernate;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;

@Setter
@Getter
@EqualsAndHashCode(exclude = {
        "user", "toUser"
})
@ToString(exclude = {
        "user", "toUser"
})
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "m_messages")
public class HibernateMessage implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private HibernateUser user;

    @NotEmpty
    @NotBlank
    @Column
    @Size(max = 5000)
    private String content;


    @Column(name = "date_of_creation")
    private Timestamp dateOfCreation;


    @Column(name = "is_read")
    private boolean isRead;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "to_user", nullable = false)
    private HibernateUser toUser;


    @Column(name = "is_deleted")
    private boolean isDeleted;
}
