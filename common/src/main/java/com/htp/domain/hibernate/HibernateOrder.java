package com.htp.domain.hibernate;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;

@Setter
@Getter
@EqualsAndHashCode(exclude = {
        "user", "country", "language"
})
@ToString(exclude = {
        "user", "country", "language"
})
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "m_orders")
public class HibernateOrder implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private HibernateUser user;

    @Column
    @NotNull
    private Long cost;

    @Column(name = "order_text")
    @NotEmpty
    @NotBlank
    @Size(max = 4000)
    private String orderText;

    @Column
    @NotNull
    private Long subscribers;


    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false)
    private HibernateCountry country;


    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "language_id", nullable = false)
    private HibernateLanguage language;

    @Column
    private Timestamp created;

    @Column(name = "is_available")

    private boolean isAvailable;

    @Column(name = "is_deleted")

    private boolean isDeleted;
}
