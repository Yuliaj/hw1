package com.htp.domain.hibernate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.checkerframework.checker.units.qual.C;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Set;

@Setter
@Getter
@RequiredArgsConstructor
@EqualsAndHashCode(exclude = {
        "roles", "qualifications", "messages", "messagesTo"
})
@ToString(exclude = {
        "roles", "qualifications", "messages", "messagesTo"
})
@AllArgsConstructor
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
//@BatchSize(size = 10)
@Table(name = "m_users")
@org.hibernate.annotations.NamedQuery(name = "HibernateUser_findAll", query = "select user from HibernateUser user order by user.id desc")
public class HibernateUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @NotBlank
    @Size(max = 100)
    @Column
    private String username;

    @NotEmpty
    @NotBlank
    @Size(max = 100)
    @Column
    private String surname;

    @Size(max = 200)
    @NotEmpty
    @NotBlank
    @Column
    private String login;

    @Size(max = 100)
    @NotEmpty
    @NotBlank
    @Column
    @JsonIgnore
    private String password;

    @NotEmpty
    @NotBlank
    @Size(max = 200)
    @Column(name = "e_mail")
    @Email
    private String email;

    @Column(name = "image_link")
    private String imageLink;


    @Column
    private Timestamp created;


    @Column
    private Timestamp changed;


    @Column(name = "is_blocked")
    private boolean isBlocked;


    @Column(name = "is_deleted")
    private boolean isDeleted;

    @JsonManagedReference
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<HibernateRole> roles = Collections.emptySet();

    @JsonManagedReference
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<HibernateQualification> qualifications = Collections.emptySet();

    @JsonManagedReference
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<HibernateMessage> messages = Collections.emptySet();

    @JsonManagedReference
    @OneToMany(mappedBy = "toUser", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<HibernateMessage> messagesTo = Collections.emptySet();
}
