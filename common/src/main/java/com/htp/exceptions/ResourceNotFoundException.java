package com.htp.exceptions;

public class ResourceNotFoundException extends RuntimeException {

    public static final String MESSAGE_WITH_ID_TEMPLATE = "%s with id %s was not found";

    public static final String MESSAGE_TEMPLATE = "%s was not found";

    public ResourceNotFoundException() {
        super();
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }

    public ResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceNotFoundException(Class<?> entityClass, Object id){
        super(String.format(MESSAGE_WITH_ID_TEMPLATE, entityClass.getSimpleName(), id));
    }
}
