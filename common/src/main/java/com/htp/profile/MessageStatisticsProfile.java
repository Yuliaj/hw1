package com.htp.profile;

public interface MessageStatisticsProfile {
    String getContent();

    Integer getCount();
}
