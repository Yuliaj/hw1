package com.htp.profile;

public interface OrderStatisticsProfile {
    String getLogin();

    Integer getCount();
}
