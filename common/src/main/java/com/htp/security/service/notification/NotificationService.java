package com.htp.security.service.notification;

import com.htp.domain.User;
import org.springframework.mail.MailException;

public interface NotificationService {

    String sendNotification(User user);
}
