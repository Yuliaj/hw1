package com.htp.security.service.notification;

import com.htp.domain.User;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@ConfigurationProperties("mail")
public class NotificationServiceImpl implements NotificationService{

    private JavaMailSender javaMailSender;

    private String from;

    public NotificationServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public String sendNotification(User user){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setFrom(from);
        mailMessage.setSubject("This is your verification code: ");
        String code = String.valueOf(RandomUtils.nextInt()/56);
        mailMessage.setText(code);
        javaMailSender.send(mailMessage);

        return code;
    }

}

