package com.htp.service.hibernate;

import com.htp.domain.hibernate.HibernateMessage;

import java.util.List;
import java.util.Optional;

public interface HibernateMessageService {
    List<HibernateMessage> findAll();

    List<HibernateMessage> search(String searchParam);

    Optional<HibernateMessage> findById(Long messageId);

    HibernateMessage findOne(Long messageId);

    HibernateMessage save(HibernateMessage message);

    HibernateMessage update(HibernateMessage message);

    int delete(Long messageId);

    int addBatch(List<HibernateMessage> messages);
}
