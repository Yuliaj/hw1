package com.htp.service.hibernate;

import com.htp.dao.hibernate.dao.HibernateMessageDao;
import com.htp.domain.hibernate.HibernateMessage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class HibernateMessageServiceImpl implements HibernateMessageService {

    HibernateMessageDao messageRepository;

    public HibernateMessageServiceImpl(HibernateMessageDao hibernateMessageDao) {
        this.messageRepository = hibernateMessageDao;
    }

    @Override
    public List<HibernateMessage> findAll() {
        return messageRepository.findAll();
    }

    @Override
    public List<HibernateMessage> search(String searchParam) {
        return messageRepository.search(searchParam);
    }

    @Override
    public Optional<HibernateMessage> findById(Long messageId) {
        return messageRepository.findById(messageId);
    }

    @Override
    public HibernateMessage findOne(Long messageId) {
        return messageRepository.findOne(messageId);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
    @Override
    public HibernateMessage save(HibernateMessage message) {
        return messageRepository.save(message);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
    @Override
    public HibernateMessage update(HibernateMessage message) {
        return messageRepository.update(message);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
    @Override
    public int delete(Long messageId) {
       return messageRepository.delete(messageId);
    }

    @Override
    public int addBatch(List<HibernateMessage> messages) {
        return 0;
    }
}
