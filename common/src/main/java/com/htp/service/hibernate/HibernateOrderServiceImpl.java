package com.htp.service.hibernate;

import com.htp.dao.hibernate.dao.HibernateOrderDao;
import com.htp.domain.hibernate.HibernateOrder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class HibernateOrderServiceImpl implements HibernateOrderService {

    private HibernateOrderDao orderRepository;

    public HibernateOrderServiceImpl(HibernateOrderDao orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public List<HibernateOrder> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public List<HibernateOrder> search(String searchParam) {
        return orderRepository.search(searchParam);
    }

    @Override
    public Optional<HibernateOrder> findById(Long orderId) {
        return orderRepository.findById(orderId);
    }

    @Override
    public HibernateOrder findOne(Long userId) {
        return orderRepository.findOne(userId);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
    @Override
    public HibernateOrder save(HibernateOrder order) {
        return orderRepository.save(order);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
    @Override
    public HibernateOrder update(HibernateOrder order) {
        return orderRepository.update(order);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
    @Override
    public int delete(Long orderId) {
        return orderRepository.delete(orderId);
    }

    @Override
    public int addBatch(List<HibernateOrder> orders) {
        return 0;
    }
}
