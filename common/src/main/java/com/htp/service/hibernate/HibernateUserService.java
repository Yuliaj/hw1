package com.htp.service.hibernate;

import com.htp.domain.hibernate.HibernateRole;
import com.htp.domain.hibernate.HibernateUser;

import java.util.List;
import java.util.Optional;

public interface HibernateUserService {
    List<HibernateUser> findAll();

    List<HibernateUser> search(String searchParam);

    Optional<HibernateUser> findById(Long userId);

    HibernateUser findOne(Long userId);

    HibernateUser save(HibernateUser user);

    HibernateUser update(HibernateUser user);

    int delete(Long userId);

    int addBatch(List<HibernateUser> users);

    Optional<List<HibernateUser>> findByLogin(String username);
}
