package com.htp.service.hibernate;

import com.htp.dao.hibernate.dao.HibernateUserDao;
import com.htp.domain.hibernate.HibernateUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class HibernateUserServiceImpl implements HibernateUserService{
    private HibernateUserDao hibernateRepository;

    public HibernateUserServiceImpl(HibernateUserDao hibernateRepository) {
        this.hibernateRepository = hibernateRepository;
    }

    @Override
    public List<HibernateUser> findAll() {
        return hibernateRepository.findAll();
    }

    @Override
    public List<HibernateUser> search(String searchParam) {
        return hibernateRepository.search(searchParam);
    }

    @Override
    public Optional<HibernateUser> findById(Long userId) {
        return hibernateRepository.findById(userId);
    }

    @Override
    public HibernateUser findOne(Long userId) {
        return hibernateRepository.findOne(userId);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
    @Override
    public HibernateUser save(HibernateUser user){
        return hibernateRepository.save(user);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
    @Override
    public HibernateUser update(HibernateUser user) {
        return hibernateRepository.update(user);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
    @Override
    public int delete(Long userId) {
       return hibernateRepository.delete(userId);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
    @Override
    public int addBatch(List<HibernateUser> users){
        return hibernateRepository.addBatch(users);
    }

    @Override
    public Optional<List<HibernateUser>> findByLogin(String username) {
        return hibernateRepository.findByLogin(username);
    }
}
