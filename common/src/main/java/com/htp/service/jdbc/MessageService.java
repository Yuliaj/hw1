package com.htp.service.jdbc;

import com.htp.domain.Message;

import java.util.List;
import java.util.Optional;

public interface MessageService {
    List<Message> findAll();

    List<Message> search(String searchParam);

    Optional<Message> findById(Long messageId);

    Message findOne(Long messageId);

    Message save(Message message);

    Message update(Message message);

    int delete(Long messageId);

    int addBatch(List<Message> messages);

}
