package com.htp.service.jdbc;

import com.htp.dao.MessageDao;
import com.htp.domain.Message;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MessageServiceImpl implements MessageService{

    private MessageDao messageDao;

    public MessageServiceImpl(@Qualifier("messageRepositoryJdbcTemplate")MessageDao messageDao) {
        this.messageDao = messageDao;
    }

    @Override
    public List<Message> findAll() {
        return messageDao.findAll();
    }

    @Override
    public List<Message> search(String searchParam) {
        return messageDao.search(searchParam);
    }

    @Override
    public Optional<Message> findById(Long messageId) {
        return Optional.ofNullable(findOne(messageId));
    }

    @Override
    public Message findOne(Long messageId) {
        return messageDao.findOne(messageId);
    }

    @Override
    public Message save(Message message) {
        return messageDao.save(message);
    }

    @Override
    public Message update(Message message) {
        return messageDao.update(message);
    }

    @Override
    public int delete(Long messageId) {
        return messageDao.delete(messageId);
    }

    @Override
    public int addBatch(List<Message> messages) {
        return messageDao.addBatch(messages);
    }
}
