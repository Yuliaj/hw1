package com.htp.service.jdbc;

import com.htp.domain.Order;

import java.util.List;
import java.util.Optional;

public interface OrderService {
    List<Order> findAll();

    List<Order> search(String searchParam);

    Optional<Order> findById(Long orderId);

    Order findOne(Long userId);

    Order save(Order order);

    Order update(Order order);

    int delete(Long orderId);

    int addBatch(List<Order> orders);
}
