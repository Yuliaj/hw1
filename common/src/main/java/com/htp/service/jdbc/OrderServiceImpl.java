package com.htp.service.jdbc;

import com.htp.dao.OrderDao;
import com.htp.domain.Order;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService{

    private OrderDao orderDao;

    public OrderServiceImpl(@Qualifier("orderRepositoryJdbcTemplate") OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Override
    public List<Order> findAll() {
        return orderDao.findAll();
    }

    @Override
    public List<Order> search(String searchParam) {
        return orderDao.search(searchParam);
    }

    @Override
    public Optional<Order> findById(Long orderId) {
        return Optional.ofNullable(findOne(orderId));
    }

    @Override
    public Order findOne(Long userId) {
        return orderDao.findOne(userId);
    }

    @Override
    public Order save(Order order) {
        return orderDao.save(order);
    }

    @Override
    public Order update(Order order) {
        return orderDao.update(order);
    }

    @Override
    public int delete(Long orderId) {
        return orderDao.delete(orderId);
    }

    @Override
    public int addBatch(List<Order> orders) {
        return orderDao.addBatch(orders);
    }
}
