package com.htp.util;

import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
public class User {
    private Long id;
    private String name;
    private String surname;
    private List<User> friends;

    public List<User> show(List<User> friends2) {
        for (User user : friends2) {
            if(user.getFriends()!=null){
                show(user.getFriends());
            }
        }
        return friends2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(friends, user.friends);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, friends);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", friends=" + friends +
                '}';
    }
}


