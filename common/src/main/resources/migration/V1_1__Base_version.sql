create table m_users
(
    id         bigserial    not null
        constraint m_users_pk
            primary key,
    username   varchar(100) not null,
    surname    varchar(100) not null,
    login      varchar(200) not null,
    password   varchar(100) not null,
    e_mail     varchar(200) not null,
    created    timestamp(6),
    changed    timestamp(6),
    is_blocked boolean      not null,
    is_deleted boolean default false
);

alter table m_users
    owner to testuser;

create unique index m_users_login_uindex
    on m_users (login);

create unique index m_users_e_mail_uindex
    on m_users (e_mail);

create table m_roles
(
    id         serial       not null
        constraint m_roles_pk
            primary key,
    role_name  varchar(100) not null,
    user_id    bigint       not null
        constraint m_roles_m_users_id_fk
            references m_users,
    is_deleted boolean default false
);

alter table m_roles
    owner to testuser;

create index m_roles_user_id_index
    on m_roles (user_id desc);

create unique index m_roles_user_id_role_name_uindex
    on m_roles (user_id, role_name);

create table m_countries
(
    id           bigserial    not null
        constraint m_countries_pk
            primary key,
    name         varchar(200) not null,
    abbreviation varchar(30)  not null,
    language     varchar(50)  not null
);

alter table m_countries
    owner to testuser;

create unique index m_countries_name_uindex
    on m_countries (name);

create unique index m_countries_abbreviation_uindex
    on m_countries (abbreviation);

create table m_languages
(
    id           bigserial    not null
        constraint m_languages_pk
            primary key,
    name         varchar(200) not null,
    abbreviation varchar(50)  not null
);

alter table m_languages
    owner to testuser;

create table m_orders
(
    id           bigserial     not null
        constraint m_orders_pk
            primary key,
    user_id      bigint        not null
        constraint m_orders_m_users_id_fk
            references m_users
            on update cascade on delete cascade,
    cost         bigint        not null,
    order_text   varchar(4000) not null,
    subscribers  bigint        not null,
    country_id   bigint        not null
        constraint m_orders_m_countries_id_fk
            references m_countries
            on update cascade on delete cascade,
    language_id  bigint
        constraint m_orders_m_languages_id_fk
            references m_languages
            on update cascade on delete cascade,
    created      timestamp(6)  not null,
    is_available boolean default true,
    is_deleted   boolean default false
);

alter table m_orders
    owner to testuser;

create unique index m_languages_name_uindex
    on m_languages (name);

create unique index m_languages_abbreviation_uindex
    on m_languages (abbreviation);

create table m_qualification
(
    id      serial       not null
        constraint m_qualification_pk
            primary key,
    name    varchar(200) not null,
    user_id bigint       not null
        constraint m_qualification_m_users_id_fk
            references m_users
            on update cascade on delete cascade
);

alter table m_qualification
    owner to testuser;

create unique index m_qualification_name_user_id_uindex
    on m_qualification (name, user_id);

create table m_messages
(
    id               bigserial     not null
        constraint m_messages_pk
            primary key,
    user_id          bigint        not null
        constraint m_messages_m_users_id_fk
            references m_users
            on update cascade on delete cascade,
    content          varchar(5000) not null,
    date_of_creation timestamp(1)  not null,
    is_read          boolean       not null,
    to_user          bigint        not null
        constraint m_messages_m_users_id_fk_2
            references m_users
            on update cascade on delete cascade,
    is_deleted       boolean default false
);

alter table m_messages
    owner to testuser;

