alter table m_users
    add image_link varchar(200) default 'image' not null;

create index m_users_image_link_index
    on m_users (image_link);
