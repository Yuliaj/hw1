package com.htp;

//import com.htp.dao.MessageDao;
//import com.htp.dao.OrderDao;
//import com.htp.dao.UserDao;
//import com.htp.domain.Message;
//import com.htp.domain.Order;
import org.apache.commons.lang3.RandomUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.sql.DataSource;
import java.awt.*;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class SpringDemo {
    private static final Logger log = Logger.getLogger(SpringDemo.class);

    public static void main(String[] args) throws SQLException {
        //ApplicationContext context = new ClassPathXmlApplicationContext("classpath:application-context.xml");
//        ApplicationContext context = new AnnotationConfigApplicationContext("com.htp");
//        UserDao userRepositoryJdbcTemplate = (UserDao) context.getBean("userRepositoryJdbcTemplate");
//        OrderDao orderRepositoryJdbcTemplate = (OrderDao) context.getBean("orderRepositoryJdbcTemplate");
//        MessageDao messageRepositoryJdbcTemplate = (MessageDao) context.getBean("messageRepositoryJdbcTemplate");
//        log.info(userRepositoryJdbcTemplate.search("1527709289"));
//        System.out.println(userRepositoryJdbcTemplate.save(new User()));
//        User user = new User();
//        /*5. Columns mapping*/
//        user.setId(RandomUtils.nextLong()/6);
//        user.setUsername(String.valueOf(RandomUtils.nextInt()));
//        user.setSurname((String.valueOf(RandomUtils.nextInt())));
//        user.setLogin((String.valueOf(RandomUtils.nextInt())));
//        user.setPassword((String.valueOf(RandomUtils.nextInt())));
//        user.setEmail((String.valueOf(RandomUtils.nextInt())));
//        user.setCreated(new Timestamp(new Date().getTime()));
//        user.setChanged(new Timestamp(new Date().getTime()));
//        user.setBlocked(false);
//        log.info(userRepositoryJdbcTemplate.save(user));
//        log.info(userRepositoryJdbcTemplate.update(userRepositoryJdbcTemplate.findOne
//                (Long.parseLong("414193860540673365"))));
//        ArrayList<User> users = new ArrayList<>();
//        for (int i = 0; i < 8; i++) {
//            User user = new User();
//            user.setId(RandomUtils.nextLong()/6);
//            user.setUsername(String.valueOf(RandomUtils.nextInt()));
//            user.setSurname((String.valueOf(RandomUtils.nextInt())));
//            user.setLogin((String.valueOf(RandomUtils.nextInt())));
//            user.setPassword((String.valueOf(RandomUtils.nextInt())));
//            user.setEmail((String.valueOf(RandomUtils.nextInt())));
//            user.setCreated(new Timestamp(new Date().getTime()));
//            user.setChanged(new Timestamp(new Date().getTime()));
//            user.setBlocked(false);
//            users.add(user);
//        }
//        log.info(users);
//        log.info(userRepositoryJdbcTemplate.addBatch(users));
//        log.info(userRepositoryJdbcTemplate.delete(Long.parseLong("1441979777928935424")));
//        log.info(userRepositoryJdbcTemplate.findOne(Long.parseLong("1441979777928935424")));
//        Message message = new Message();
////        message.setId(RandomUtils.nextLong()/9);
//        message.setUserId(Long.parseLong("23"));
//        message.setContent("something");
//        message.setDateOfCreation(new Timestamp(new Date().getTime()));
//        message.setRead(false);
//        message.setTo(Long.parseLong("8"));
//        log.info(messageRepositoryJdbcTemplate.save(message));
//        log.info(messageRepositoryJdbcTemplate.findOne(Long.parseLong("1")));
//        log.info(messageRepositoryJdbcTemplate.findAll());
//        for (Message message : messageRepositoryJdbcTemplate.findAll()) {
//            //System.out.println(user);
//            log.info(message.getContent());
//        }
//        Message message = messageRepositoryJdbcTemplate.findOne(Long.parseLong("4"));
//        message.setContent("sgdfgf");
//        message.setId(Long.parseLong("4"));
//        log.info(messageRepositoryJdbcTemplate.update(message).getContent());
//        log.info(messageRepositoryJdbcTemplate.delete(Long.parseLong("2")));
//        ArrayList<Message> messages = new ArrayList<>();
//        for (int i = 0; i < 8; i++) {
//            Message message = new Message();
//            message.setId(RandomUtils.nextLong()/9);
//            message.setUserId(Long.parseLong("23"));
//            message.setContent("something");
//            message.setDateOfCreation(new Timestamp(new Date().getTime()));
//            message.setRead(false);
//            message.setTo(Long.parseLong("8"));
//            messages.add(message);
//        }
//        log.info(messages);
//        log.info(messageRepositoryJdbcTemplate.addBatch(messages));
//        Order order = orderRepositoryJdbcTemplate.findOne(Long.parseLong("7"));
//        order.setUserId(Long.parseLong("24"));
//        order.setCost(RandomUtils.nextLong());
//        order.setOrderText(String.valueOf(RandomUtils.nextInt()));
////        log.info(orderRepositoryJdbcTemplate.update(order).getOrderText());
//        order.setSubscribers(RandomUtils.nextLong()/23);
//        order.setCountryId(Long.parseLong("1"));
//        order.setLanguageId(Long.parseLong("1"));
//        order.setCreated(new Timestamp(new Date().getTime()));
//        order.setAvailable(true);
////        order.setId(RandomUtils.nextLong());
//        log.info(orderRepositoryJdbcTemplate.save(order));
//        log.info(userRepositoryJdbcTemplate.delete(Long.parseLong("24")));
//        log.info(orderRepositoryJdbcTemplate.delete(Long.parseLong("7")));
//        log.info(messageRepositoryJdbcTemplate.delete(Long.parseLong("4")));
//        log.info(userRepositoryJdbcTemplate.findOne(Long.parseLong("23")));

    }
}
