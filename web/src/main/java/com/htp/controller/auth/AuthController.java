package com.htp.controller.auth;

import com.htp.controller.request.auth.AuthRequest;
import com.htp.controller.request.auth.AuthRequestWithoutCode;
import com.htp.controller.request.auth.AuthResponse;
//import com.htp.controller.request.create.hibernate.UserCreateRequestHibernate;
import com.htp.controller.request.create.UserCreateRequest;
import com.htp.dao.RoleDao;
import com.htp.domain.Role;
import com.htp.domain.Roles;
import com.htp.domain.User;
import com.htp.security.service.notification.NotificationService;
import com.htp.service.jdbc.UserService;
import com.htp.security.util.TokenUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;

@Slf4j
@RestController
@RequestMapping("/auth")
public class AuthController {

    private TokenUtils tokenUtils;

    private AuthenticationManager authenticationManager;

    private UserDetailsService userDetailsService;

    private UserService userService;

    private RoleDao roleDao;

    private NotificationService notificationService;

    Long benchMark;

    String code;

    public AuthController(TokenUtils tokenUtils, AuthenticationManager authenticationManager,
                          @Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService, UserService userService,
                          RoleDao roleDao, NotificationService notificationService) {
        this.tokenUtils = tokenUtils;
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.userService = userService;
        this.roleDao = roleDao;
        this.notificationService = notificationService;
    }

    @ApiOperation(value = "Login user by username and password")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful login user"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @PostMapping
    public ResponseEntity<AuthResponse> login (@Valid @RequestBody AuthRequest authRequest){

            Authentication authenticate = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            authRequest.getUsername(),
                            authRequest.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authenticate);
        AuthResponse build = AuthResponse
                .builder()
                .login(authRequest.getUsername())
                .jwtToken(tokenUtils.generateToken(userDetailsService.loadUserByUsername(authRequest.getUsername())))
                .build();
        //to ensure that code is valid
        try {
            boolean get = code.equals(authRequest.getVerificationCode());
            if(get && System.currentTimeMillis() - benchMark < 60000){
                return new ResponseEntity<>(build, HttpStatus.OK);
            } else{
                return new ResponseEntity<>(AuthResponse.builder()
                        .login("your verification code expired. Please, go to verification " +
                                "code sender to get another one." +
                                "Please note that verification code only lasts 1 minute")
                        .build(), HttpStatus.UNAUTHORIZED);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @ApiOperation(value = "Get verification code")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful getting code"),
            @ApiResponse(code = 422, message = "Failed sending code properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @PostMapping("registration/getCode")
    public ResponseEntity<String> sendCode(@RequestBody AuthRequestWithoutCode requestWithoutCode) {
        User user = userService.findByLogin(requestWithoutCode.getUsername());
        try {
            code = notificationService.sendNotification(user);
            benchMark = System.currentTimeMillis();
        } catch (MailException e){
            log.error("Error Mail: " + e.getMessage());
        }
        return new ResponseEntity<>( "verification was sent just now, it will last 1 minute", HttpStatus.OK);
    }

    @ApiOperation(value = "Create user")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful creation user"),
            @ApiResponse(code = 422, message = "Failed user creation properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @PostMapping("registration")
    public User create(@Valid @RequestBody UserCreateRequest createRequest) {

        User user = new User();
        user.setUsername(createRequest.getUsername());
        user.setSurname(createRequest.getSurname());
        user.setLogin(createRequest.getLogin());
        user.setPassword(createRequest.getPassword());
        user.setEmail(createRequest.getEmail());
        user.setCreated(new Timestamp(new Date().getTime()));
        user.setChanged(new Timestamp(new Date().getTime()));
        user.setBlocked(false);
        user.setDeleted(false);

        User savedUser = userService.save(user);
        Role role = new Role();
        role.setRoleName(Roles.ROLE_USER.name());
        role.setUserId(savedUser.getId());
        Role savedRole = roleDao.save(role);
        savedUser.setRole(savedRole);

        try {
            code = notificationService.sendNotification(user);
            benchMark = System.currentTimeMillis();
        } catch (MailException e){
            log.error("Error Mail: " + e.getMessage());
        }

        return savedUser;
    }
}
