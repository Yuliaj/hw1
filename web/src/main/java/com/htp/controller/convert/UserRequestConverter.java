package com.htp.controller.convert;

import com.htp.controller.request.create.UserCreateRequest;
import com.htp.domain.hibernate.HibernateUser;
import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;
import java.util.Date;

@Slf4j
public abstract class UserRequestConverter<S, T> extends EntityConverter<S, T> {

    protected HibernateUser doConvert(HibernateUser user, UserCreateRequest request) {


        user.setUsername(request.getUsername());
        user.setEmail(request.getEmail());
        user.setSurname(request.getSurname());
        user.setLogin(request.getLogin());
        user.setPassword(request.getPassword());
        user.setChanged(new Timestamp(new Date().getTime()));
        user.setImageLink("image");

        return user;
    }
}
