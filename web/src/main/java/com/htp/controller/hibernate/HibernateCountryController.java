package com.htp.controller.hibernate;

import com.htp.dao.hibernate.CountryHibernateRepository;
import com.htp.domain.hibernate.HibernateCountry;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/hibernate/countries")
public class HibernateCountryController {
    private final CountryHibernateRepository countryHibernateRepository;

    public HibernateCountryController(CountryHibernateRepository countryHibernateRepository) {
        this.countryHibernateRepository = countryHibernateRepository;
    }
    @ApiOperation(value = "Finding all countries")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading countries"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @GetMapping
    public ResponseEntity<List<HibernateCountry>> findAll() {
        return new ResponseEntity<>(countryHibernateRepository.findAll(), HttpStatus.OK);
    }
}
