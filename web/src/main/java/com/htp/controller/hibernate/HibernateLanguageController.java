package com.htp.controller.hibernate;

import com.htp.dao.hibernate.LanguageHibernateRepository;
import com.htp.domain.hibernate.HibernateLanguage;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/hibernate/languages")
public class HibernateLanguageController {
    private final LanguageHibernateRepository languageHibernateRepository;

    public HibernateLanguageController(LanguageHibernateRepository languageHibernateRepository) {
        this.languageHibernateRepository = languageHibernateRepository;
    }

    @ApiOperation(value = "Finding all languages")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading languages"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @GetMapping
    public ResponseEntity<List<HibernateLanguage>> findAll() {
        return new ResponseEntity<>(languageHibernateRepository.findAll(), HttpStatus.OK);
    }
}
