package com.htp.controller.hibernate;

import com.htp.controller.request.create.MessageCreateRequest;

import com.htp.controller.request.update.MessageUpdateRequest;
import com.htp.dao.hibernate.dao.HibernateMessageDao;
import com.htp.domain.hibernate.HibernateMessage;
import com.htp.domain.hibernate.HibernateUser;
import com.htp.service.hibernate.HibernateMessageService;
import com.htp.service.hibernate.HibernateUserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@Transactional
@RequestMapping("/hibernate/messages")
public class HibernateMessageController {
    private HibernateMessageService hibernateMessageDao;

    private HibernateUserService hibernateUserService;

    public HibernateMessageController(HibernateMessageService hibernateMessageDao, HibernateUserService hibernateUserService) {
        this.hibernateMessageDao = hibernateMessageDao;
        this.hibernateUserService = hibernateUserService;
    }

    @ApiOperation(value = "Finding all messages")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading messages"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @GetMapping
    public ResponseEntity<List<HibernateMessage>> findAll() {
        return new ResponseEntity<>(hibernateMessageDao.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Finding message by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading message"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "id", value = "Message database id", example = "7",
                    required = true, dataType = "long", paramType = "path")
    })
    @GetMapping("/{id}")
    @Secured("ADMIN")
    public HibernateMessage findById(@PathVariable("id") Long messageId) {
        return hibernateMessageDao.findOne(messageId);
    }

    @ApiOperation(value = "Search message by date")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading message"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "query", value = "Search query - words",
                    example = "hey", required = true, dataType = "String", paramType = "query")
    })
    @GetMapping("/search")
    public List<HibernateMessage> searchMessage(@RequestParam("query") String query) {
        return hibernateMessageDao.search(query);
    }

    @ApiOperation(value = "Create message")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful creation message"),
            @ApiResponse(code = 422, message = "Failed message creation properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @PostMapping
    public HibernateMessage create(@Valid @RequestBody MessageCreateRequest createRequest) {

        HibernateMessage message = new HibernateMessage();
        message.setUser(hibernateUserService.findOne(createRequest.getUserId()));
        message.setContent(createRequest.getContent());
        message.setDateOfCreation(new Timestamp(new Date().getTime()));
        message.setRead(false);
        message.setToUser(hibernateUserService.findOne(createRequest.getToUser()));
        message.setDeleted(false);
        return hibernateMessageDao.save(message);
    }

    @ApiOperation(value = "Update message")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful updating message"),
            @ApiResponse(code = 422, message = "Failed message updating properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @PatchMapping("/update/{id}")
    public HibernateMessage update(@Valid @RequestBody MessageUpdateRequest updateRequest, @PathVariable("id") Long messageId) {
        HibernateMessage message = hibernateMessageDao.findOne(messageId);
        message.setContent(updateRequest.getContent());
        return hibernateMessageDao.update(message);
    }

    @ApiOperation(value = "Deleting message by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful deleting message"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "id", value = "Message database id", example = "7",
                    required = true, dataType = "long", paramType = "path")
    })
    @DeleteMapping("/delete/{id}")
    public int delete(@PathVariable("id") Long messageId){
        return hibernateMessageDao.delete(messageId);
    }

}
