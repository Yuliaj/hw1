package com.htp.controller.hibernate;

import com.htp.controller.request.create.OrderCreateRequest;

import com.htp.controller.request.update.OrderUpdateRequest;
import com.htp.dao.hibernate.CountryHibernateRepository;
import com.htp.dao.hibernate.LanguageHibernateRepository;
import com.htp.domain.hibernate.HibernateOrder;
import com.htp.service.hibernate.HibernateOrderService;
import com.htp.service.hibernate.HibernateUserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@Transactional
@RequestMapping("/hibernate/orders")
public class HibernateOrderController {
    private HibernateOrderService hibernateOrderDao;

    private HibernateUserService hibernateUserService;

    private LanguageHibernateRepository languageHibernateRepository;

    private CountryHibernateRepository countryHibernateRepository;

    public HibernateOrderController(HibernateOrderService hibernateOrderDao,
                                    HibernateUserService hibernateUserService,
                                    LanguageHibernateRepository languageHibernateRepository,
                                    CountryHibernateRepository countryHibernateRepository) {
        this.hibernateOrderDao = hibernateOrderDao;
        this.hibernateUserService = hibernateUserService;
        this.languageHibernateRepository = languageHibernateRepository;
        this.countryHibernateRepository = countryHibernateRepository;
    }

    @ApiOperation(value = "Finding all orders")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading orders"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header")
    })
    @GetMapping
    public ResponseEntity<List<HibernateOrder>> findAll() {
        return new ResponseEntity<>(hibernateOrderDao.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Finding order by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading order"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "id", value = "Order database id", example = "8", required = true, dataType = "long", paramType = "path")
    })
    @GetMapping("/{id}")
    public HibernateOrder findById(@PathVariable("id") Long orderId) {
        return hibernateOrderDao.findOne(orderId);
    }

    @ApiOperation(value = "Search orders by cost")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading order"),
            @ApiResponse(code = 404, message = "Server error, something wrong"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "query", value = "Search query - cost, that you would like",
                    example = "1000", required = true, dataType = "int", paramType = "query")
    })
    @GetMapping("/search")
    public List<HibernateOrder> searchOrder(@RequestParam("query") String query) {
        return hibernateOrderDao.search(query);
    }

    @ApiOperation(value = "Create order")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful creation order"),
            @ApiResponse(code = 422, message = "Failed order creation properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @PostMapping
    public HibernateOrder create(@Valid @RequestBody OrderCreateRequest createRequest) {

        HibernateOrder order = new HibernateOrder();
        order.setUser(hibernateUserService.findOne(createRequest.getUserId()));
        order.setCost(createRequest.getCost());
        order.setOrderText(createRequest.getOrderText());
        order.setSubscribers(createRequest.getSubscribers());
        order.setCountry(countryHibernateRepository.findOne(createRequest.getCountryId()));
        order.setLanguage(languageHibernateRepository.findOne(createRequest.getLanguageId()));
        order.setCreated(new Timestamp(new Date().getTime()));
        order.setAvailable(true);
        order.setDeleted(false);
        return hibernateOrderDao.save(order);
    }

    @ApiOperation(value = "Update order")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful updating order"),
            @ApiResponse(code = 422, message = "Failed order updating properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @PatchMapping("/update/{id}")
    public HibernateOrder update(@Valid @RequestBody OrderUpdateRequest updateRequest, @PathVariable("id") Long orderId) {
        HibernateOrder order = hibernateOrderDao.findOne(orderId);
        order.setCost(updateRequest.getCost());
        order.setOrderText(updateRequest.getOrderText());
        order.setSubscribers(updateRequest.getSubscribers());
        return hibernateOrderDao.update(order);
    }

    @ApiOperation(value = "Deleting order by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful deleting order"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header")
    })
    @DeleteMapping("/delete/{id}")
    public int delete(@PathVariable("id") Long orderId){
        return hibernateOrderDao.delete(orderId);
    }

}
