package com.htp.controller.hibernate;

import com.htp.dao.hibernate.QualificationHibernateRepository;
import com.htp.domain.hibernate.HibernateQualification;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/hibernate/qualifications")
public class HibernateQualificationController {
    private final QualificationHibernateRepository qualificationHibernateRepository;

    public HibernateQualificationController(QualificationHibernateRepository qualificationHibernateRepository) {
        this.qualificationHibernateRepository = qualificationHibernateRepository;
    }

    @ApiOperation(value = "Finding all qualifications")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading qualifications"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @GetMapping
    public ResponseEntity<List<HibernateQualification>> findAll() {
        return new ResponseEntity<>(qualificationHibernateRepository.findAll(), HttpStatus.OK);
    }
}
