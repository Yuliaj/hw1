package com.htp.controller.hibernate;

import com.htp.dao.hibernate.dao.HibernateRoleDao;
import com.htp.domain.hibernate.HibernateRole;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Transactional
@RequestMapping("/hibernate/roles")
public class HibernateRoleController {

    private HibernateRoleDao hibernateRoleDao;

    public HibernateRoleController(HibernateRoleDao hibernateRoleDao) {
        this.hibernateRoleDao = hibernateRoleDao;
    }

    @ApiOperation(value = "Finding all roles")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading roles"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header")
    })
    @GetMapping
    public ResponseEntity<List<HibernateRole>> findAll() {
        return new ResponseEntity<>(hibernateRoleDao.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Finding role by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading role"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "id", value = "User database id", example = "7", required = true, dataType = "long", paramType = "path")
    })
    @GetMapping("/{id}")
    public HibernateRole findById(@PathVariable("id") Long userId) {
        return hibernateRoleDao.findOne(userId);
    }

    @ApiOperation(value = "Find role by user id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading role"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
    })
    @GetMapping("/search")
    public List<HibernateRole> findByUserId(@RequestParam("id") Long userId) {
        return hibernateRoleDao.findByUserId(userId);
    }

    @ApiOperation(value = "Find role by roleName")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading role"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
    })
    @GetMapping("/search/role")
    public List<HibernateRole> findByRoleName(@RequestParam("roleName") String search) {
        return hibernateRoleDao.search(search);
    }

}
