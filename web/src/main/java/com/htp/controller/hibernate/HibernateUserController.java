package com.htp.controller.hibernate;


import com.htp.controller.request.create.UserCreateRequest;
import com.htp.controller.request.update.UserUpdateRequest;
import com.htp.domain.Roles;
import com.htp.domain.hibernate.HibernateRole;
import com.htp.domain.hibernate.HibernateUser;
import com.htp.service.hibernate.HibernateUserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@Transactional
@RequestMapping("/hibernate/users")
public class HibernateUserController {

    private HibernateUserService hibernateUserService;


    public HibernateUserController(HibernateUserService hibernateUserDao) {
        this.hibernateUserService = hibernateUserDao;
    }

    @ApiOperation(value = "Finding all users")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading users"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header")
    })
    @GetMapping
    public ResponseEntity<List<HibernateUser>> findAll() {
        return new ResponseEntity<>(hibernateUserService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Finding user by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading user"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "id", value = "User database id", example = "7", required = true, dataType = "long", paramType = "path")
    })
    @GetMapping("/{id}")
    public HibernateUser findById(@PathVariable("id") Long userId) {
        return hibernateUserService.findOne(userId);
    }

    @ApiOperation(value = "Search users by login")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading user"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "query", value = "Search query - free text", example = "viachaslauk", required = true, dataType = "string", paramType = "query")
    })
    @GetMapping("/search")
    public Optional<List<HibernateUser>> searchUser(@RequestParam("query") String query) {
        return hibernateUserService.findByLogin(query);
    }

    @ApiOperation(value = "Updating user")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful updating user"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
//            @ApiImplicitParam(name = "User", value = "User", example = "7",
//                    required = true, dataType = "long", paramType = "path")
    })
    @PatchMapping("/update/{id}")
    public HibernateUser update(@Valid @RequestBody UserUpdateRequest updateRequest, @PathVariable("id") Long userId) throws Exception {
        HibernateUser user = hibernateUserService.findOne(userId);
        user.setLogin(updateRequest.getLogin());
        user.setPassword(updateRequest.getPassword());
        user.setEmail(updateRequest.getEmail());
        user.setChanged(new Timestamp(new Date().getTime()));
        return hibernateUserService.update(user);
    }

    @ApiOperation(value = "Deleting user by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful deleting user"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "id", value = "User database id", example = "7",
                    required = true, dataType = "long", paramType = "path")
    })
    @DeleteMapping("/delete/{id}")
    public int delete(@PathVariable("id") Long userId){
        return hibernateUserService.delete(userId);
    }

    @ApiOperation(value = "Create user")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful creation user"),
            @ApiResponse(code = 422, message = "Failed user creation properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header")
    })
    @PostMapping("registration")
    public HibernateUser create(@Valid @RequestBody UserCreateRequest createRequest) throws Exception {

        HibernateUser user = new HibernateUser();
        user.setUsername(createRequest.getUsername());
        user.setSurname(createRequest.getSurname());
        user.setLogin(createRequest.getLogin());
        user.setPassword(createRequest.getPassword());
        user.setEmail(createRequest.getEmail());
        user.setCreated(new Timestamp(new Date().getTime()));
        user.setChanged(new Timestamp(new Date().getTime()));
        user.setBlocked(false);
        user.setDeleted(false);
        user.setImageLink("image");

        HibernateRole hibernateRole = new HibernateRole();
        hibernateRole.setRoleName(Roles.ROLE_USER.name());
        hibernateRole.setUser(user);
        user.setRoles(Collections.singleton(hibernateRole));

        return hibernateUserService.save(user);
    }

    @ApiOperation(value = "Add bunch of users")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful creation users"),
            @ApiResponse(code = 422, message = "Failed user creation properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header")
    })
    @PostMapping("batch")
    public int addBatch(@Valid @RequestBody List<UserCreateRequest> createRequestHibernate) throws Exception {
        List<HibernateUser> userList = new ArrayList<>();
        for (UserCreateRequest users: createRequestHibernate) {
            HibernateUser user = new HibernateUser();
            user.setUsername(users.getUsername());
            user.setSurname(users.getSurname());
            user.setLogin(users.getLogin());
            user.setPassword(users.getPassword());
            user.setEmail(users.getEmail());
            user.setCreated(new Timestamp(new Date().getTime()));
            user.setChanged(new Timestamp(new Date().getTime()));
            user.setBlocked(false);
            user.setDeleted(false);
            user.setImageLink("image");
            userList.add(user);
            HibernateRole hibernateRole = new HibernateRole();
            hibernateRole.setRoleName(Roles.ROLE_USER.name());
            hibernateRole.setUser(user);
            user.setRoles(Collections.singleton(hibernateRole));
        }
        return hibernateUserService.addBatch(userList);
    }


}
