package com.htp.controller.jdbc;

import com.htp.controller.request.create.MessageCreateRequest;
import com.htp.domain.Message;
import com.htp.service.jdbc.MessageService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/messages")
public class MessageController {

    private MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @ApiOperation(value = "Finding all messages")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading messages"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @GetMapping
    public ResponseEntity<List<Message>> findAll() {
        return new ResponseEntity<>(messageService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Finding message by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading message"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "id", value = "Message database id", example = "7",
                    required = true, dataType = "long", paramType = "path")
    })
    @GetMapping("/{id}")
    @Secured("ADMIN")
    public Message findById(@PathVariable("id") Long messageId) {
        return messageService.findOne(messageId);
    }

    @ApiOperation(value = "Search message by date")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading message"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "query", value = "Search query - words",
                    example = "hey", required = true, dataType = "String", paramType = "query")
    })
    @GetMapping("/search")
    public List<Message> searchMessage(@RequestParam("query") String query) {
        return messageService.search(query);
    }

    @ApiOperation(value = "Create message")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful creation message"),
            @ApiResponse(code = 422, message = "Failed message creation properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @PostMapping
    public Message create(@Valid @RequestBody MessageCreateRequest createRequest) {

        Message message = new Message();
        message.setUserId(createRequest.getUserId());
        message.setContent(createRequest.getContent());
        message.setDateOfCreation(new Timestamp(new Date().getTime()));
        message.setRead(false);
        message.setTo(createRequest.getToUser());
        message.setDeleted(false);
        return messageService.save(message);
    }

    @ApiOperation(value = "Deleting message by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful deleting message"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "id", value = "Message database id", example = "7",
                    required = true, dataType = "long", paramType = "path")
    })
    public void delete(@PathVariable("id") Long messageId){
        messageService.delete(messageId);
    }

}
