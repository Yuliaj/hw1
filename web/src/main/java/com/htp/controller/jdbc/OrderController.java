package com.htp.controller.jdbc;

import com.htp.controller.request.create.OrderCreateRequest;
import com.htp.domain.Order;
import com.htp.service.jdbc.OrderService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @ApiOperation(value = "Finding all orders")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading orders"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header")
    })
    @GetMapping
    public ResponseEntity<List<Order>> findAll() {
        return new ResponseEntity<>(orderService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Finding order by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading order"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "id", value = "Order database id", example = "8", required = true, dataType = "long", paramType = "path")
    })
    @GetMapping("/{id}")
    public Order findById(@PathVariable("id") Long orderId) {
        return orderService.findOne(orderId);
    }

    @ApiOperation(value = "Search orders by cost")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading order"),
            @ApiResponse(code = 404, message = "Server error, something wrong"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "query", value = "Search query - cost, that you would like",
                    example = "1000", required = true, dataType = "int", paramType = "query")
    })
    @GetMapping("/search")
    public List<Order> searchOrder(@RequestParam("query") String query) {
        return orderService.search(query);
    }

    @ApiOperation(value = "Create order")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful creation order"),
            @ApiResponse(code = 422, message = "Failed order creation properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @PostMapping
    public Order create(@Valid @RequestBody OrderCreateRequest createRequest) {

        Order order = new Order();
        order.setUserId(createRequest.getUserId());
        order.setCost(createRequest.getCost());
        order.setOrderText(createRequest.getOrderText());
        order.setSubscribers(createRequest.getSubscribers());
        order.setCountryId(createRequest.getCountryId());
        order.setLanguageId(createRequest.getLanguageId());
        order.setCreated(new Timestamp(new Date().getTime()));
        order.setAvailable(true);
        order.setDeleted(false);
        return orderService.save(order);
    }

    @ApiOperation(value = "Deleting order by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful deleting order"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "Order database id", example = "7",
                    required = true, dataType = "long", paramType = "path")
    })
    public void delete(Long orderId){
        orderService.delete(orderId);
    }

}
