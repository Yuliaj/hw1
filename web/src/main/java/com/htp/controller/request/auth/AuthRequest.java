package com.htp.controller.request.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@ApiModel(description = "Authentication object with login and password and verification code")
public class AuthRequest implements Serializable {

    @NotEmpty
    @ApiModelProperty(required = true, allowableValues = "joshuaaa", dataType = "string")
    private String username;

    @NotEmpty
    @ApiModelProperty(required = true , allowableValues = "ys87h3d8d", dataType = "string")
    private String password;

    @NotEmpty
    @Size(max = 10)
    @ApiModelProperty(required = true, allowableValues = "444", dataType = "string")
    private String verificationCode;
}
