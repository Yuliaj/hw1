package com.htp.controller.request.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel(description = "Authentication object with login and password")
public class AuthRequestWithoutCode {

    @NotEmpty
    @ApiModelProperty(required = true, allowableValues = "joshuaaa", dataType = "string")
    private String username;

    @NotEmpty
    @ApiModelProperty(required = true , allowableValues = "ys87h3d8d", dataType = "string")
    private String password;
}
