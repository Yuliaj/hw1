package com.htp.controller.request.create;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
//@Data
@Builder
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class OrderCreateRequest {

    @NotNull
    private Long userId;

    @NotNull
    private Long cost;

    @NotEmpty
    @NotBlank
    private String orderText;

    @NotNull
    private Long subscribers;

    @NotNull
    private Long countryId;

    @NotNull
    private Long languageId;
}
