package com.htp.controller.request.update;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class OrderUpdateRequest {

    @NotNull
    private Long cost;

    @NotEmpty
    @NotBlank
    private String orderText;

    @NotNull
    private Long subscribers;


}
