package com.htp.controller.spring_data;

import com.htp.dao.spring_data.SpringDataMessageRepository;
import com.htp.dao.spring_data.SpringDataUserRepository;
import com.htp.profile.MessageStatisticsProfile;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RequestMapping("/spring_data/messages")
@RestController
@Transactional
public class SpringDataMessageController {

    SpringDataMessageRepository dataMessageRepository;
    SpringDataUserRepository springDataUserRepository;

    public SpringDataMessageController(SpringDataMessageRepository dataMessageRepository,
                                       SpringDataUserRepository springDataUserRepository) {
        this.dataMessageRepository = dataMessageRepository;
        this.springDataUserRepository = springDataUserRepository;
    }

    @ApiOperation(value = "Counting matches")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful counting"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @GetMapping
    public ResponseEntity<Integer> countMatches(String string) {
        return new ResponseEntity<>(dataMessageRepository.countHibernateMessageByContentContains(string), HttpStatus.OK);
    }

    @ApiOperation(value = "Getting statistics")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful getting statistics"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @GetMapping("/statistics")
    public ResponseEntity<List<MessageStatisticsProfile>> getStatistics(Long userId) {
        return new ResponseEntity<>(dataMessageRepository.getHibernateMessageStatisticsForUser(userId), HttpStatus.OK);
    }

    @ApiOperation(value = "Getting statistics for all")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful getting statistics"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @GetMapping("/statisticsOverall")
    public ResponseEntity<List<MessageStatisticsProfile>> getStatisticsOverall() {
        return new ResponseEntity<>(dataMessageRepository.getHibernateMessageStatisticsOverall(), HttpStatus.OK);
    }

}
