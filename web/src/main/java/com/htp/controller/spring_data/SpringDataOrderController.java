package com.htp.controller.spring_data;

import com.htp.dao.spring_data.SpringDataOrderRepository;
import com.htp.profile.OrderStatisticsProfile;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/spring_data/orders")
@RestController
@Transactional
public class SpringDataOrderController {

    SpringDataOrderRepository springDataOrderRepository;

    public SpringDataOrderController(SpringDataOrderRepository springDataOrderRepository) {
        this.springDataOrderRepository = springDataOrderRepository;
    }

    @ApiOperation(value = "Getting statistics")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful getting statistics"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @GetMapping("/statistics")
    public ResponseEntity<OrderStatisticsProfile> getStatistics(Long userId) {
        return new ResponseEntity<>(springDataOrderRepository.getHibernateOrderStatisticsForUser(userId), HttpStatus.OK);
    }

    @ApiOperation(value = "Getting statistics")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful getting statistics"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    @GetMapping("/statisticsForAll")
    public ResponseEntity<List<OrderStatisticsProfile>> getStatisticsForAllUsers() {
        return new ResponseEntity<>(springDataOrderRepository.getHibernateOrderStatisticsForAllUsers(), HttpStatus.OK);
    }

}
