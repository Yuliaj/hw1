package com.htp.controller.spring_data;

import com.htp.amazon.AmazonUploadingFileService;
import com.htp.controller.request.ResetPassword;
import com.htp.controller.request.create.UserCreateRequest;
import com.htp.dao.spring_data.SpringDataUserRepository;
import com.htp.domain.hibernate.HibernateUser;
import com.htp.exceptions.ResourceNotFoundException;
import com.htp.security.util.PrincipalUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;


@Slf4j
@RestController
@Transactional
@RequestMapping("/spring_data/users")
public class SpringDataUserController {
    private SpringDataUserRepository springDataUserRepository;

    private ConversionService conversionService;

    private AmazonUploadingFileService amazonUploadingFileService;

    public SpringDataUserController(SpringDataUserRepository springDataUserRepository,
                                    ConversionService conversionService,
                                    AmazonUploadingFileService amazonUploadingFileService) {
        this.springDataUserRepository = springDataUserRepository;
        this.conversionService = conversionService;
        this.amazonUploadingFileService = amazonUploadingFileService;
    }

    @ApiOperation(value = "Search with pagination")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading users"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "Page number", example = "0", defaultValue = "0", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "Items per page", example = "3", defaultValue = "3", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "Field to sort", example = "0", defaultValue = "id", dataType = "string", paramType = "query")
    })
    @GetMapping("/search")
    public ResponseEntity<Page<HibernateUser>> searchWithPagination(@ApiIgnore Pageable pageable) {
        Page<HibernateUser> usersPage = springDataUserRepository.findAll(pageable);
        return new ResponseEntity<>(usersPage, HttpStatus.OK);
    }

    @ApiOperation(value = "Create user")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful creation user"),
            @ApiResponse(code = 422, message = "Failed user creation properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
    @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
            dataType = "string", paramType = "header")
    })
    @PostMapping
    public HibernateUser create(@Valid @RequestBody UserCreateRequest createRequest){
        HibernateUser user = conversionService.convert(createRequest, HibernateUser.class);
        return springDataUserRepository.save(Objects.requireNonNull(user));
    }

    //business-case number 1
    @ApiOperation(value = "Reset password")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful resetting password"),
            @ApiResponse(code = 422, message = "Failed password validation properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header")
    })
    @PatchMapping("/reset")
    public ResponseEntity<Integer> resetMyPassword(@Valid @RequestBody ResetPassword resetPassword,
                                                   Principal principal){
        String username = PrincipalUtil.getUsername(principal);
        HibernateUser user = springDataUserRepository.findByLoginIs(username);
        if(resetPassword.getOldPassword().equals(user.getPassword())){
            return new ResponseEntity<>(springDataUserRepository.resetPassword(resetPassword.getNewPassword(),
                    user.getId()), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    /*business case number 2*/
    @ApiOperation(value = "Upload photo to profile")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful uploading photo"),
            @ApiResponse(code = 422, message = "Failed uploading photo"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true,
                    dataType = "string", paramType = "header")
    })
    @PutMapping("/photo/{id}")
    public ResponseEntity<Map<Long, String>> uploadPhotoToProfile(@PathVariable("id") String userId, MultipartFile multipartFile){
        Long parsedUserId = Long.parseLong(userId);
        HibernateUser user = springDataUserRepository.findById(parsedUserId)
                .orElseThrow(() -> new ResourceNotFoundException(HibernateUser.class, parsedUserId));

        try {
            byte[] imageBytes = multipartFile.getBytes();
            String imageLink = amazonUploadingFileService.uploadFile(imageBytes, parsedUserId);
            user.setImageLink(imageLink);
            springDataUserRepository.save(user);
            return new ResponseEntity<>(Collections.singletonMap(user.getId(), user.getImageLink()), HttpStatus.OK);
        } catch (IOException e) {
            throw new RuntimeException("Image uploading Error!");
        }
    }
}


